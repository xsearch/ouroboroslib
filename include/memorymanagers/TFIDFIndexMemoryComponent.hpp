#ifndef OUROBOROS_TFIDF_INDEX_MEMORY_COMPONENT_H
#define OUROBOROS_TFIDF_INDEX_MEMORY_COMPONENT_H

#include "memorymanagers/BaseMemoryComponent.hpp"
#include "indexing/information/BaseTFIDFIndex.hpp"
#include "corestructures/HugePagedBytesStore.hpp"
#include <memory>

namespace ouroboros
{
    enum class TFIDFIndexMemoryComponentType {STD, DENSE, SWISS, CHAINED, PSTD, PDENSE, PSWISS, PCHAINED};

    class TFIDFIndexMemoryComponent: public BaseMemoryComponent
    {
        private:
            std::shared_ptr<HugePagedBytesStore> bytesStore;
            std::shared_ptr<BaseTFIDFIndex> index;
        public:
            explicit TFIDFIndexMemoryComponent(long pageSize,
                                              TFIDFIndexMemoryComponentType type,
                                              unsigned long numBuckets = 134217728);
            virtual ~TFIDFIndexMemoryComponent() = default;

            virtual std::shared_ptr<HugePagedBytesStore> getPagedBytesStore();
            virtual std::shared_ptr<BaseTFIDFIndex> getTFIDFIndex();
    };
}

#endif