#ifndef OUROBOROS_TOKEN_STORE_MEMORY_COMPONENT_H
#define OUROBOROS_TOKEN_STORE_MEMORY_COMPONENT_H

#include "memorymanagers/BaseMemoryComponent.hpp"
#include "corestructures/BasePagedStringStore.hpp"

#include <deque>

namespace ouroboros
{
    class TokenStoreMemoryComponent: public BaseMemoryComponent
    {
        private:
            BasePagedStringStore* store;
            std::deque<char*>* tokens;
        public:
            TokenStoreMemoryComponent(long pageSize);
            virtual ~TokenStoreMemoryComponent();

            TokenStoreMemoryComponent(const TokenStoreMemoryComponent& other) = delete;
            TokenStoreMemoryComponent& operator=(const TokenStoreMemoryComponent& other) = delete;
            TokenStoreMemoryComponent(TokenStoreMemoryComponent&& other) = default;
            TokenStoreMemoryComponent& operator=(TokenStoreMemoryComponent&& other) = default;

            virtual BasePagedStringStore* getPagedStringStore();
            virtual std::deque<char*>* getTokens();
    };
}

#endif