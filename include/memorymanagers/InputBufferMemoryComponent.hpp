#ifndef OUROBOROS_INPUTBUFFER_MEMORY_COMPONENT_H
#define OUROBOROS_INPUTBUFFER_MEMORY_COMPONENT_H

#include "memorymanagers/BaseMemoryComponent.hpp"
#include <vector>
#include "readerdrivers/FileDataBlock.hpp"

namespace ouroboros
{
    class InputBufferMemoryComponent: public BaseMemoryComponent
    {
        private:
            char* buffer;
            bool allocated;
            long bufferSize;
            std::vector<FileDataBlock*>* blockVector;
        public:
            InputBufferMemoryComponent();
            virtual ~InputBufferMemoryComponent();
            virtual std::vector<FileDataBlock*>* getBlocks();

            virtual bool isAllocated();
    };
}

#endif
