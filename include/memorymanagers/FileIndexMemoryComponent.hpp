#ifndef OUROBOROS_FILE_INDEX_MEMORY_COMPONENT_H
#define OUROBOROS_FILE_INDEX_MEMORY_COMPONENT_H

#include "memorymanagers/BaseMemoryComponent.hpp"
#include "indexing/sources/BaseFileIndex.hpp"
#include <memory>

namespace ouroboros
{
    enum class FileIndexMemoryComponentType {STD};

    class FileIndexMemoryComponent: public BaseMemoryComponent
    {
        private:
            std::shared_ptr<BaseFileIndex> index;
        public:
            explicit FileIndexMemoryComponent(FileIndexMemoryComponentType type,
                                              unsigned long numBuckets = 134217728);
            virtual ~FileIndexMemoryComponent() = default;

            virtual std::shared_ptr<BaseFileIndex> getFileIndex();
    };
}

#endif