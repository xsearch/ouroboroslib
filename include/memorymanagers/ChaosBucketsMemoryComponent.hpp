#ifndef OUROBOROS_CHAOS_BUCKETS_MEMORY_COMPONENT_H
#define OUROBOROS_CHAOS_BUCKETS_MEMORY_COMPONENT_H

#include "memorymanagers/BaseMemoryComponent.hpp"
#include "corestructures/ChaosHashTable.hpp"

namespace ouroboros
{
    class ChaosBucketsMemoryComponent: public BaseMemoryComponent
    {
        private:
            static const long SMALL_PAGE_ALIGNMENT = 4096;
            static const long LARGE_PAGE_ALIGNMENT = 2097152;
            static const long HUGE_PAGE_ALIGNMENT = 1073741824;

            ChaosBucket<char*, long>* buckets;
            long numBuckets;
            long alignment;
        public:
            ChaosBucketsMemoryComponent(long numBuckets);
            virtual ~ChaosBucketsMemoryComponent();

            virtual ChaosBucket<char*, long>* getChaosBuckets();
            virtual void configRandomAccess();
            virtual void configSequentialAccess();
    };
}

#endif