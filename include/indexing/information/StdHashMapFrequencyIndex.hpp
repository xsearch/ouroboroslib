#ifndef OUROBOROS_STDHASHMAPFREQUENCYINDEX_H
#define OUROBOROS_STDHASHMAPFREQUNECYINDEX_H

#include <unordered_map>
#include <mutex>

#include "indexing/information/BaseFrequencyIndex.hpp"

namespace ouroboros
{
    class StdHashMapFrequencyIndex: public BaseFrequencyIndex
    {
        protected:
            std::unordered_map<long, long> *map;
            std::mutex mtx;
            long size;
        public:
            StdHashMapFrequencyIndex();
            virtual ~StdHashMapFrequencyIndex();

            virtual void insert(long idx);
            virtual void remove(long idx);
            virtual long lookup(long idx);
            virtual void initialize(long idx, long value);
            virtual void increment(long idx, long value);
            virtual void decrement(long idx, long value);
            virtual long getSize();
    };
}

#endif