#ifndef OUROBOROS_BASEINFORMATIONINDEX_H
#define OUROBOROS_BASEINFORMATIONINDEX_H

namespace ouroboros
{
    enum class InformationIndexType {BASE, FREQUENCY, DIRECTTFIDF, TFIDF};

    class BaseInformationIndex
    {
        protected:
            InformationIndexType type;
        public:
            BaseInformationIndex(InformationIndexType type) : type(type) { }
            virtual ~BaseInformationIndex() = default;
            InformationIndexType getType() { return type; }
    };
}

#endif