#ifndef OUROBOROS_STD_DIRECTTFIDF_INDEX_H
#define OUROBOROS_STD_DIRECTTFIDF_INDEX_H

#include "indexing/indexing.hpp"
#include "indexing/information/BaseInformationIndex.hpp"

#include <unordered_map>
#include <vector>
#include <tuple>

namespace ouroboros
{
    class StdDirectTFIDFIndex: BaseInformationIndex
    {
        struct tfidf_entry_t {
            std::string* term;
            long fileIdx;
            long frequency;
        };

        private:
            std::unordered_map<std::string*, long, strptr_hash, strptr_equal>* map;
            std::vector<tfidf_entry_t*>* terms;
        public:
            StdDirectTFIDFIndex();
            virtual ~StdDirectTFIDFIndex();

            virtual long insert(const char *term, long fileIdx);
            virtual long lookup(const char *term);
            virtual const std::tuple<const char*, long, long> reverseLookup(long idx);
    };
}

#endif