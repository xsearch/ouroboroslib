#ifndef OUROBOROS_PAGED_STD_MAP_TFIDF_INDEX_H
#define OUROBOROS_PAGED_STD_MAP_TFIDF_INDEX_H

#include "indexing/information/BaseTFIDFIndex.hpp"
#include "indexing/information/TFIndexEntry.hpp"
#include "corestructures/HugePagedBytesStore.hpp"
#include "corestructures/PagedVersatileIndex.hpp"
#include "indexing/indexing.hpp"
#include <unordered_map>

namespace ouroboros
{
    class PagedStdMapTFIDFIndex: public BaseTFIDFIndex
    {
        private:
            std::shared_ptr<HugePagedBytesStore> bytesStore;
            std::unordered_map<const char*, PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>*, cstr_hash, cstr_equal> map;
            long numTerms;
        public:
            explicit PagedStdMapTFIDFIndex(std::shared_ptr<HugePagedBytesStore> bytesStore) : 
                bytesStore(bytesStore), map(), numTerms(0) { }
            virtual ~PagedStdMapTFIDFIndex() = default;

            virtual void insert(const char *term, long fileIdx);
            virtual std::shared_ptr<TFIndexResult> lookup(const char *term);
            virtual long getNumTerms();
    };
}

#endif