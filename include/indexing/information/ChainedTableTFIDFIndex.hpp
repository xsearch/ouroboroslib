#ifndef OUROBOROS_PAGED_CHAINED_TABLE_TFIDF_INDEX_H
#define OUROBOROS_PAGED_CHAINED_TABLE_TFIDF_INDEX_H

#include "indexing/information/BaseTFIDFIndex.hpp"
#include "indexing/information/TFIndexEntry.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include "corestructures/PagedVersatileStore.hpp"
#include "corestructures/PagedVersatileIndex.hpp"
#include "corestructures/PagedLinkedElement.hpp"
#include "corestructures/ChainedHashTable.hpp"
#include "indexing/indexing.hpp"
#include <deque>

namespace ouroboros
{
    class PagedChainedTableTFIDFIndex: public BaseTFIDFIndex
    {
        private:
            std::shared_ptr<BasePagedStringStore> termStore;
            std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>>> indexStore;
            std::shared_ptr<PagedVersatileStore<PagedLinkedElement<IDFIndexEntry>>> listStore;
            ChainedHashTable<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>*> index;
            long numTerms;
        public:
            explicit PagedChainedTableTFIDFIndex(std::shared_ptr<BasePagedStringStore> termStore,
                std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>>> idxStor,
                std::shared_ptr<PagedVersatileStore<PagedLinkedElement<IDFIndexEntry>>> lstStor,
                unsigned long numBuckets) : termStore(termStore), indexStore(idxStor), listStore(lstStor),
                map(numBuckets), index(), numTerms(0) { }
            virtual ~PagedChainedTableTFIDFIndex() = default;

            virtual long insert(const char *term, long fileIdx);
            virtual long lookup(const char *term);
            virtual std::shared_ptr<TFIndexResult> reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif