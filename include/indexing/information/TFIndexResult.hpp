#ifndef OUROBOROS_TF_INDEX_RESULT_H
#define OUROBOROS_TF_INDEX_RESULT_H

#include "IDFIndexEntry.hpp"
#include <deque>
#include <cstddef>

namespace ouroboros
{
    struct TFIndexResult
    {
        const char* term;
        long termFrequency;
        std::deque<IDFIndexEntry> files;

        TFIndexResult() : term(NULL), termFrequency(0), files() { }
        TFIndexResult(const char* term, long termFrequency) : term(term), termFrequency(termFrequency), files() { }
        ~TFIndexResult() = default;
    };
}

#endif
