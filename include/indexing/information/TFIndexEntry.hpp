#ifndef OUROBOROS_TF_INDEX_ENTRY_H
#define OUROBOROS_TF_INDEX_ENTRY_H

namespace ouroboros
{
    struct TFIndexEntry
    {
        const char* term;
        long termFrequency;
        long fileIdx;
        long fileFrequency;

        TFIndexEntry() : term(NULL), termFrequency(-1), fileIdx(-1), fileFrequency(-1) { }
        TFIndexEntry(const char* term, long termFrequency, long fileIdx, long fileFrequency) : 
            term(term), termFrequency(termFrequency), fileIdx(fileIdx), fileFrequency(fileFrequency) { }
        ~TFIndexEntry() = default;
    };
}

#endif