#ifndef OUROBOROS_STD_DECK_FILE_INDEX_H
#define OUROBOROS_STD_DECK_FILE_INDEX_H

#include "indexing/sources/BaseFileIndex.hpp"
#include <deque>

namespace ouroboros
{
    class StdDeckFileIndex: public BaseFileIndex
    {
        private:
            std::deque<const char*> files;
            long idx;
        public:
            explicit StdDeckFileIndex() : files(std::deque<const char*>()), idx(0) { }
            virtual ~StdDeckFileIndex() = default;

            virtual long insert(const char *path);
            virtual long lookup(const char *path);
            virtual const char* reverseLookup(long idx);
    };
}

#endif