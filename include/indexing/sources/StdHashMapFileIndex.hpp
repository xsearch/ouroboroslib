#ifndef OUROBOROS_STDHASHMAPFILEINDEX_H
#define OUROBOROS_STDHASHMAPFILEINDEX_H

#include <unordered_map>
#include <vector>
#include <string>
#include <mutex>

#include "indexing/sources/BaseFileIndex.hpp"
#include "indexing/indexing.hpp"

namespace ouroboros
{
    class StdHashMapFileIndex: public BaseFileIndex
    {
        protected:
            std::unordered_map<std::string*, long, strptr_hash, strptr_equal> *map;
            std::vector<std::string*> *index;
            std::mutex mtx;
            long size;
        public:
            StdHashMapFileIndex();
            virtual ~StdHashMapFileIndex();

            virtual long insert(const char *path);
            virtual void remove(const char *path);
            virtual long lookup(const char *path);
            virtual const char* reverseLookup(long idx);
            virtual long getSize();
    };
}

#endif