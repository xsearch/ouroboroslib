#ifndef OUROBOROS_CHAINED_TABLE_TERM_FILE_INDEX_H
#define OUROBOROS_CHAINED_TABLE_TERM_FILE_INDEX_H

#include "indexing/knowledge/BaseTermFileIndex.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include "corestructures/ChainedHashTable.hpp"
#include "indexing/indexing.hpp"
#include <deque>

namespace ouroboros
{
    class ChainedTableTermFileIndex: public BaseTermFileIndex
    {
        private:
            std::shared_ptr<BasePagedStringStore> store;
            ChainedHashTable<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<std::shared_ptr<TermFileIndexEntry>> index;
            long numTerms;
        public:
            explicit ChainedTableTermFileIndex(std::shared_ptr<BasePagedStringStore> store, unsigned long numBuckets) : 
                store(store), map(numBuckets), index(), numTerms(0) { }
            virtual ~ChainedTableTermFileIndex() = default;

            virtual long insert(const char *term, long fileIdx);
            virtual void remove(const char *term, long fileIdx);
            virtual long lookup(const char *term);
            virtual std::shared_ptr<TermFileIndexEntry> reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif