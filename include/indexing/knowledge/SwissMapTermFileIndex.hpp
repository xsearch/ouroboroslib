#ifndef OUROBOROS_SWISS_MAP_TERM_FILE_INDEX_H
#define OUROBOROS_SWISS_MAP_TERM_FILE_INDEX_H

#ifdef GOOGLE_SWISS_LIB

#include "indexing/knowledge/BaseTermFileIndex.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include "indexing/indexing.hpp"
#include <unordered_map>
#include <deque>

#include <absl/container/flat_hash_map.h>

namespace ouroboros
{
    class SwissMapTermFileIndex: public BaseTermFileIndex
    {
        private:
            std::shared_ptr<BasePagedStringStore> store;
            absl::flat_hash_map<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<std::shared_ptr<TermFileIndexEntry>> index;
            long numTerms;
        public:
            explicit SwissMapTermFileIndex(std::shared_ptr<BasePagedStringStore> store) : 
                store(store), map(), index(), numTerms(0) { }
            virtual ~SwissMapTermFileIndex() = default;

            virtual long insert(const char *term, long fileIdx);
            virtual void remove(const char *term, long fileIdx);
            virtual long lookup(const char *term);
            virtual std::shared_ptr<TermFileIndexEntry> reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif

#endif