#ifndef OUROBOROS_PAGED_CHAINED_TABLE_TERM_FILE_INDEX_H
#define OUROBOROS_PAGED_CHAINED_TABLE_TERM_FILE_INDEX_H

#include "indexing/knowledge/BaseTermFileIndex.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include "corestructures/PagedVersatileStore.hpp"
#include "corestructures/PagedVersatileIndex.hpp"
#include "corestructures/PagedLinkedElement.hpp"
#include "corestructures/ChainedHashTable.hpp"
#include "indexing/indexing.hpp"
#include <deque>

namespace ouroboros
{
    class PagedChainedTableTermFileIndex: public BaseTermFileIndex
    {
        private:
            std::shared_ptr<BasePagedStringStore> termStore;
            std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<char*, long>>> indexStore;
            std::shared_ptr<PagedVersatileStore<PagedLinkedElement<long>>> listStore;
            ChainedHashTable<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<PagedVersatileIndex<char*, long>*> index;
            long numTerms;
        public:
            explicit PagedChainedTableTermFileIndex(std::shared_ptr<BasePagedStringStore> termStore,
                std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<char*, long>>> idxStor,
                std::shared_ptr<PagedVersatileStore<PagedLinkedElement<long>>> lstStor,
                unsigned long numBuckets) : termStore(termStore), indexStore(idxStor), listStore(lstStor),
                map(numBuckets), index(), numTerms(0) { }
            virtual ~PagedChainedTableTermFileIndex() = default;

            virtual long insert(const char *term, long fileIdx);
            virtual void remove(const char *term, long fileIdx);
            virtual long lookup(const char *term);
            virtual std::shared_ptr<TermFileIndexEntry> reverseLookup(long idx);
            virtual PagedVersatileIndex<char*, long>* nativeReverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif