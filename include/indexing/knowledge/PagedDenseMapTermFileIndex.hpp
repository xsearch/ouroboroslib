#ifndef OUROBOROS_PAGED_DENSE_MAP_TERM_FILE_INDEX_H
#define OUROBOROS_PAGED_DENSE_MAP_TERM_FILE_INDEX_H

#ifdef GOOGLE_DENSE_LIB

#include "indexing/knowledge/BaseTermFileIndex.hpp"
#include "corestructures/BasePagedStringStore.hpp"
#include "corestructures/PagedVersatileStore.hpp"
#include "corestructures/PagedVersatileIndex.hpp"
#include "corestructures/PagedLinkedElement.hpp"
#include "indexing/indexing.hpp"
#include <deque>

#include <sparsehash/dense_hash_map>

namespace ouroboros
{
    class PagedDenseMapTermFileIndex: public BaseTermFileIndex
    {
        private:
            std::shared_ptr<BasePagedStringStore> termStore;
            std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<char*, long>>> indexStore;
            std::shared_ptr<PagedVersatileStore<PagedLinkedElement<long>>> listStore;
            google::dense_hash_map<const char*, long, cstr_hash, cstr_equal> map;
            std::deque<PagedVersatileIndex<char*, long>*> index;
            long numTerms;
        public:
            explicit PagedDenseMapTermFileIndex(std::shared_ptr<BasePagedStringStore> termStore,
                std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<char*, long>>> idxStor,
                std::shared_ptr<PagedVersatileStore<PagedLinkedElement<long>>> lstStor) : termStore(termStore),
                indexStore(idxStor), listStore(lstStor), map(), index(), numTerms(0) { map.set_empty_key(NULL); }
            virtual ~PagedDenseMapTermFileIndex() = default;

            virtual long insert(const char *term, long fileIdx);
            virtual void remove(const char *term, long fileIdx);
            virtual long lookup(const char *term);
            virtual std::shared_ptr<TermFileIndexEntry> reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif

#endif