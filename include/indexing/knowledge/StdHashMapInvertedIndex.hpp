#ifndef OUROBOROS_STDHASHMAPINVERTEDINDEX_H
#define OUROBOROS_STDHASHMAPINVERTEDINDEX_H

#include <unordered_map>
#include <vector>
#include <mutex>

#include "indexing/knowledge/BaseTermFileRelation.hpp"

namespace ouroboros
{
    class StdHashMapInvertedIndex: public BaseTermFileRelation
    {
        protected:
            std::unordered_map<long, std::unordered_map<long, long>*> *map;
            std::vector<std::tuple<long, long>> *index;
            std::mutex mtx;
            long size;
        public:
            StdHashMapInvertedIndex();
            virtual ~StdHashMapInvertedIndex();

            virtual long insert(long termIdx, long fileIdx);
            virtual void remove(long termIdx, long fileIdx);
            std::vector<std::tuple<long, long>>* lookup(long termIdx);
            virtual void reverseRemove(long idx);
            virtual std::tuple<long, long> reverseLookup(long idx);
            virtual long getSize();
    };
}

#endif