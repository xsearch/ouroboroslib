#ifndef OUROBOROS_BASEDATAINDEX_H
#define OUROBOROS_BASEDATAINDEX_H

namespace ouroboros
{
    enum class DataIndexType {BASE, TERM};

    class BaseDataIndex
    {
        protected:
            DataIndexType type;
        public:
            BaseDataIndex(DataIndexType type) : type(type) { }
            virtual ~BaseDataIndex() = default;
            DataIndexType getType() { return type; }
    };
}

#endif