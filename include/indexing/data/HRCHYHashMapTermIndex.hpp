#ifndef OUROBOROS_HRCHYHASHMAPTERMINDEX_H
#define OUROBOROS_HRCHYHASHMAPTERMINDEX_H

#include <unordered_map>
#include <vector>
#include <string>
#include <mutex>
#include <atomic>

#include "indexing/data/BaseTermIndex.hpp"
#include "indexing/indexing.hpp"

namespace ouroboros
{
    class HRCHYVector
    {
        protected:
            std::vector<std::vector<std::string*>*> *index;
            std::mutex mtx;
            std::atomic<long> pos;
            long capacity;
            int vectorWidth;
        public:
            HRCHYVector(int vectorWidth);
            virtual ~HRCHYVector();

            virtual long push_back(std::string *term);
            virtual std::string* at(long idx);
            virtual void toombstone(long idx);
    };

    class HRCHYHashMapTermIndex: public BaseTermIndex
    {
        protected:
            std::vector<std::unordered_map<std::string*, long, strptr_hash, strptr_equal>*> *maps;
            std::vector<std::mutex*> *mtxs;
            HRCHYVector *index;
            std::atomic<long> numTerms;
            int mapWidth;
        public:
            HRCHYHashMapTermIndex(int mapWidth, int vectorWidth);
            virtual ~HRCHYHashMapTermIndex();

            unsigned long djb2_hash(const unsigned char *str);
            virtual int hash(const char *str);

            virtual long insert(const char *term);
            virtual void remove(const char *term);
            virtual long lookup(const char *term);
            virtual const char* reverseLookup(long idx);
            virtual long getNumTerms();
    };
}

#endif