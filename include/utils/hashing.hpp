#ifndef OUROBOROS_HASHING_H
#define OUROBOROS_HASHING_H

inline unsigned long djb2_hash(const unsigned char *str)
{
    long hash = 5381;

    for (auto i = 0; str[i] != '\0'; i++) {
        hash = ((hash << 5) + hash) + ((int) str[i]);
    }

    return hash;
}

#endif