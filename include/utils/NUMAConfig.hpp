#ifndef OUROBOROS_NUMA_CONFIG_H
#define OUROBOROS_NUMA_CONFIG_H

#include <iostream>

extern "C"
{
    #include <numa.h>
}

#define OUROBOROS_NONUMA -1

namespace ouroboros
{
    class NUMAConfig
    {
        private:
            int num_numa_nodes = OUROBOROS_NONUMA;
        public:
            NUMAConfig();
            ~NUMAConfig() = default;

            int get_num_numa_nodes();
            void set_numa_node(int node);
            void set_task_numa_node(int task);
    };

    NUMAConfig::NUMAConfig() {
        if (numa_available() < 0) {
            std::cerr << "WARN: NUMA is not available on the current system!" << std::endl;
        } else {
            num_numa_nodes = numa_max_node() + 1;
        }
    }

    int NUMAConfig::get_num_numa_nodes() {
        return num_numa_nodes;
    }

    void NUMAConfig::set_numa_node(int node) {
        if (num_numa_nodes != OUROBOROS_NONUMA) {
            if (numa_run_on_node(node) < 0) {
                std::cerr << "WARN: Could not set task to run on NUMA node!" << std::endl;
            }
            numa_set_preferred(node);
        }
    }

    void NUMAConfig::set_task_numa_node(int task) {
        int node;

        if (num_numa_nodes != OUROBOROS_NONUMA) {
            node = task % num_numa_nodes;
            if (numa_run_on_node(node) < 0) {
                std::cerr << "WARN: Could not set task to run on NUMA node!" << std::endl;
            }
            numa_set_preferred(node);
        }
    }
}

#endif