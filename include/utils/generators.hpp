#ifndef OUROBOROS_GENERATORS_H
#define OUROBOROS_GENERATORS_H

#include <vector>
#include <tuple>

std::tuple<std::vector<char*>*, char*> generate_synthdata(long num_terms, int term_size)
{
    char *buffer;
    std::vector<char*> *terms;
    char term_elem[term_size + 1];

    for (auto i = 0; i < (term_size + 1); ++i) {
        term_elem[i] = 'A';
    }

    terms = new std::vector<char*>();
    buffer = new char[num_terms * (term_size + 1)];
    for (auto i = 0; i < num_terms; ++i) {
        for (auto j = 0; j < term_size; ++j) {
            buffer[i * (term_size + 1) + j] = term_elem[j];
            
            if (j == 0) {
                term_elem[j] += 1;
            }

            if (term_elem[j] == 'Z') {
                term_elem[j + 1] += 1;
                term_elem[j] = 'a';
            }

            if (term_elem[j] == 'z') {
                term_elem[j + 1] += 1;
                term_elem[j] = 'A';
            }
        }
        buffer[i * (term_size + 1) + term_size] = '\0';
        terms->push_back(&buffer[i * (term_size + 1)]);
    }

    return std::make_tuple(terms, buffer);
}

std::tuple<std::vector<char*>*, char*, std::vector<std::size_t>*> generate_synthdatahash(long num_terms, int term_size)
{
    char *buffer;
    std::vector<char*> *terms;
    std::vector<std::size_t> *hashes;
    char term_elem[term_size + 1];

    for (auto i = 0; i < (term_size + 1); ++i) {
        term_elem[i] = 'A';
    }

    terms = new std::vector<char*>();
    hashes = new std::vector<std::size_t>();
    buffer = new char[num_terms * (term_size + 1)];
    for (auto i = 0; i < num_terms; ++i) {
        for (auto j = 0; j < term_size; ++j) {
            buffer[i * (term_size + 1) + j] = term_elem[j];
            
            if (j == 0) {
                term_elem[j] += 1;
            }

            if (term_elem[j] == 'Z') {
                term_elem[j + 1] += 1;
                term_elem[j] = 'a';
            }

            if (term_elem[j] == 'z') {
                term_elem[j + 1] += 1;
                term_elem[j] = 'A';
            }
        }
        buffer[i * (term_size + 1) + term_size] = '\0';
        terms->push_back(&buffer[i * (term_size + 1)]);
        hashes->push_back(0);
    }

    return std::make_tuple(terms, buffer, hashes);
}

#endif