#ifndef OUROBOROS_BASE_STORAGE_DRIVER_H
#define OUROBOROS_BASE_STORAGE_DRIVER_H

#include "readerdrivers/BaseDataBlock.hpp"

namespace ouroboros
{
    enum class ReaderDriverType {BASE, FILE, PFILE, AIOFILE, WFILE};

    class BaseReaderDriver
    {
        private:
            ReaderDriverType type;
        public:
            BaseReaderDriver(ReaderDriverType type) : type(type) { }
            virtual ~BaseReaderDriver() = default;

            ReaderDriverType getType() { return type; }
            
            virtual void open() = 0;
            virtual void close() = 0;
            virtual void readNextBlock(BaseDataBlock*) = 0;
    };
}

#endif