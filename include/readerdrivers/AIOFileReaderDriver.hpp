#ifndef OUROBOROS_AIOFILEREADERDRIVER_H
#define OUROBOROS_AIOFILEREADERDRIVER_H

#include "readerdrivers/BaseReaderDriver.hpp"
#include "readerdrivers/FileDataBlock.hpp"

#include <string>
#include <exception>

namespace linuxIO
{
    extern "C" {
        #include <aio.h>

        typedef struct aiocb aiocb_t;
    }
}

namespace ouroboros
{
    class AIOFileReaderException: public std::exception
    {
        private:
            std::string msg;
        public:
            AIOFileReaderException(const std::string s) : msg(s) { }
            virtual const char* what() const throw();
            ~AIOFileReaderException() = default;
    };

    class AIOFileReaderDriver: public BaseReaderDriver
    {
        private:
            char *filepath;
            int position;
            int blockSize;
            char *cacheBuffer;
            linuxIO::aiocb_t *aiocbs;
            linuxIO::aiocb_t **aiocbsList;
            int cachePosition;
            int cacheSize;
        public:
            AIOFileReaderDriver(char *filepath, int blockSize, char *buffer, int cacheNumBlocks);
            virtual void open();
            virtual void close();
            virtual void readNextBlock(BaseDataBlock *dataBlock);
            virtual void readNextBlock(FileDataBlock *dataBlock);
            ~AIOFileReaderDriver();
    };
}

#endif