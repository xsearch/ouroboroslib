#ifndef OUROBOROS_BASEDATABLOCK_H
#define OUROBOROS_BASEDATABLOCK_H

namespace ouroboros
{
    enum class DataBlockType {BASE, FILE};

    class BaseDataBlock
    {
        protected:
            DataBlockType type;
        public:
            BaseDataBlock(DataBlockType type) : type(type) { }
            DataBlockType getType() { return type; }
            virtual ~BaseDataBlock() = default;
    };
}

#endif