#ifndef OUROBOROS_BASETOKBLOCK_H
#define OUROBOROS_BASETOKBLOCK_H

namespace ouroboros
{
    enum class TokBlockType {BASE, C};

    class BaseTokBlock
    {
        protected:
            TokBlockType type;
        public:
            BaseTokBlock(TokBlockType type) : type(type) { }
            TokBlockType getType() { return type; }
            virtual ~BaseTokBlock() = default;
    };
}

#endif