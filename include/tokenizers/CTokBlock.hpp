#ifndef OUROBOROS_CTOKBLOCK_H
#define OUROBOROS_CTOKBLOCK_H

#include "tokenizers/BaseTokBlock.hpp"

namespace ouroboros
{
    class CTokBLock: public BaseTokBlock
    {
        public:
            char *buffer;
            char **tokens;
            long numTokens;
            CTokBLock();
            ~CTokBLock() = default;
    };
}

#endif