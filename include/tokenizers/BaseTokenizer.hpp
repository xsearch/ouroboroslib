#ifndef OUROBOROS_BASETOKENIZER_H
#define OUROBOROS_BASETOKENIZER_H

#include "readerdrivers/BaseDataBlock.hpp"
#include "tokenizers/BaseTokBlock.hpp"

namespace ouroboros
{
    enum class TokenizerType {BASE, STANDARD, BRANCHLESS};

    class BaseTokenizer
    {
        protected:
            TokenizerType type;
        public:
            BaseTokenizer(TokenizerType type) : type(type) { }
            TokenizerType getType() { return type; }
            virtual void getTokens(BaseDataBlock *, BaseTokBlock *) = 0;
            virtual ~BaseTokenizer() = default;
    };
}
#endif