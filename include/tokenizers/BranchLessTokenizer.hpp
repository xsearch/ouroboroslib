#ifndef OUROBOROS_BRANCHLESSTOKENIZER_H
#define OUROBOROS_BRANCHLESSTOKENIZER_H

#include "tokenizers/BaseTokenizer.hpp"
#include "tokenizers/CTokBlock.hpp"
#include "readerdrivers/FileDataBlock.hpp"

namespace ouroboros
{
    class BranchLessTokenizer: public BaseTokenizer
    {
        private:
            char *delim;
            char charDict[256];
        public:
            BranchLessTokenizer(char *delim);
            virtual void getTokens(BaseDataBlock *dataBlock, BaseTokBlock *tokBlock);
            virtual void getTokens(FileDataBlock *dataBlock, CTokBLock *tokBlock);
            ~BranchLessTokenizer() = default;
    };
}

#endif