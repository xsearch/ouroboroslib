#ifndef OUROBOROSLIB_FILESTORAGEMANAGER_H
#define OUROBOROSLIB_FILESTORAGEMANAGER_H

#include <iostream>
#include <string>

#include "stormanagers/BaseStorageManager.hpp"

namespace ouroboros {
    class FileStorageManager: public BaseStorageManager {
        private:
            std::string filename;
            std::fstream filep();
            int bufferSize;
            int blockSize;
            int blockNum;
        public:
            FileStorageManager(std::string, int, int);
            void open(const std::string);
            smblock_t readBlock();
            void writeBlock(std::string);
            void close();
    };
}

#endif