#ifndef OUROBOROSLIB_BASESTORAGEMANAGER_H
#define OUROBOROSLIB_BASESTORAGEMANAGER_H

#include <string>

namespace ouroboros
{
    typedef struct smblock {
        std::string block;
        int blockNum;
        int blockLength;
    } smblock_t;

    class BaseStorageManager {
        public:
            virtual void open(const std::string) =0;
            virtual smblock_t readBlock() =0;
            virtual void writeBlock(std::string) =0;
            virtual void close() =0;
    };
}

#endif