#ifndef OUROBOROS_SIMPLEHASHMAPV2_H
#define OUROBOROS_SIMPLEHASHMAPV2_H

#include <functional>

namespace ouroboros
{
    class SimpleHashMapV2
    {
        protected:
            long **buckets;
            unsigned long numBuckets;
            std::hash<char*> hashFn;
        public:
            SimpleHashMapV2(unsigned long numBuckets);
            virtual ~SimpleHashMapV2();

            virtual unsigned long hash(char* str);
            virtual void insert(char *key, long value);
            virtual long lookup(char *key);
    };
}

#endif