#ifndef OUROBOROS_SIMPLEHASHMAPV3_H
#define OUROBOROS_SIMPLEHASHMAPV3_H

#include "corestructures/BucketManager.hpp"

#include <functional>

namespace ouroboros
{
    class SimpleHashMapV3
    {
        protected:
            bucket_t<char*, long>* buckets;
            BucketManager<char*, long>* bucketManager;
            unsigned long numBuckets;
            std::hash<char*> hashFn;
        public:
            SimpleHashMapV3(unsigned long numBuckets, BucketManager<char*, long>* bucketManager);
            virtual ~SimpleHashMapV3();

            virtual unsigned long hash(char* str);
            virtual void insert(char *key, long value);
            virtual long lookup(char *key);
    };
}

#endif