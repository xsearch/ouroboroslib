#ifndef OUROBOROS_BASE_PAGED_STRING_STORE_H
#define OUROBOROS_BASE_PAGED_STRING_STORE_H

namespace ouroboros
{
    enum class PagedStringStoreType {BASE, DEFAULT, HUGE, RANDOM};

    class BasePagedStringStore
    {
        private:
            PagedStringStoreType type;
        public:
            BasePagedStringStore(PagedStringStoreType type) : type(type) { }
            virtual ~BasePagedStringStore() = default;

            virtual PagedStringStoreType getType() { return type; }
            virtual void expand() = 0;
            virtual char* store(const char* str) = 0;
    };
}

#endif