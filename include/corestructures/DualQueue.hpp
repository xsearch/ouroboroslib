#ifndef OUROBOROS_DUALQUEUE_H
#define OUROBOROS_DUALQUEUE_H

#include "corestructures/SyncQueue.hpp"

#include <vector>

namespace ouroboros
{   
    template <class T>
    class DualQueue
    {
        protected:
            int maxSize;
            SyncQueue<T> *full_slots;
            SyncQueue<T> *empty_slots;
        public:
            DualQueue(int maxSize);
            ~DualQueue();
            void push_full(T elem);
            void push_empty(T elem);
            T pop_full();
            T pop_empty();
    };

    template<class T>
    DualQueue<T>::DualQueue(int maxSize) {
        this->maxSize = maxSize;
        full_slots = new SyncQueue<T>(maxSize);
        empty_slots = new SyncQueue<T>(maxSize);
    }

    template<class T>
    DualQueue<T>::~DualQueue() {
        delete full_slots;
        delete empty_slots;
    }

    template<class T>
    void DualQueue<T>::push_full(T elem) {
        full_slots->push(elem);
    }

    template<class T>
    void DualQueue<T>::push_empty(T elem) {
        empty_slots->push(elem);
    }

    template<class T>
    T DualQueue<T>::pop_full() {
        return full_slots->pop();
    }

    template<class T>
    T DualQueue<T>::pop_empty() {
        return empty_slots->pop();
    }
}

#endif