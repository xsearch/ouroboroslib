#ifndef OUROBOROS_BUCKETMANAGER_H
#define OUROBOROS_BUCKETMANAGER_H

#include <vector>

#define OUROBOROS_DEFAULT_BUCKET_CHUNKSIZE 4096

namespace ouroboros
{
    template<class K, class V>
    struct bucket_t
    {
        K key;
        V value;
        bucket_t<K, V> *next;
    };

    template<class K, class V>
    class BucketManager
    {
        protected:
            std::vector<bucket_t<K, V>*>* buckets;
            long capacity;
            long capacityStep;
            long numBuckets;
        public:
            BucketManager();
            BucketManager(long capacityStep);
            virtual ~BucketManager();

            virtual void increaseaCapacity();
            virtual bucket_t<K, V>* getNewBucket();
            virtual long getCapacity();
            virtual long getNumBuckets();
    };

    template<class K, class V>
    BucketManager<K, V>::BucketManager()
    {
        buckets = new std::vector<bucket_t<K, V>*>();
        capacityStep = OUROBOROS_DEFAULT_BUCKET_CHUNKSIZE;
        capacity = 0;
        numBuckets = 0;
        increaseaCapacity();
    }

    template<class K, class V>
    BucketManager<K, V>::BucketManager(long capacityStep)
    {
        buckets = new std::vector<bucket_t<K, V>*>();
        this->capacityStep = capacityStep;
        capacity = 0;
        numBuckets = 0;
        increaseaCapacity();
    }

    template<class K, class V>
    BucketManager<K, V>::~BucketManager()
    {
        for (bucket_t<K, V>*& bucket : (*buckets)) {
            delete bucket;
        }
        delete buckets;
    }

    template<class K, class V>
    void BucketManager<K, V>::increaseaCapacity()
    {
        bucket_t<K, V>* newBucket;
        newBucket = new bucket_t<K, V>[capacityStep]{};
        buckets->push_back(newBucket);
        capacity += capacityStep;
    }

    template<class K, class V>
    bucket_t<K, V>* BucketManager<K, V>::getNewBucket()
    {
        long pos = numBuckets++;
        long i = pos / capacityStep;
        long j = pos % capacityStep;
        bucket_t<K, V> *bucket;

        if (numBuckets >= capacity) {
            increaseaCapacity();
        }

        bucket = &((*buckets)[i][j]);
        return bucket;
    }

    template<class K, class V>
    long BucketManager<K, V>::getCapacity()
    {
        return capacity;
    }

    template<class K, class V>
    long BucketManager<K, V>::getNumBuckets()
    {
        return numBuckets;
    }

}

#endif