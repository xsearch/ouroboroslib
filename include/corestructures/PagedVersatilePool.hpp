#ifndef OUROBOROS_PAGED_VERSATILE_POOL_H
#define OUROBOROS_PAGED_VERSATILE_POOL_H

extern "C"
{
    #include <sys/mman.h>
}

#include <cstdlib>
#include <deque>
#include <new>

namespace ouroboros
{
    template<class T>
    class PagedVersatilePool
    {
        private:
            static const long SMALL_PAGE_ALIGNMENT = 4096;
            static const long LARGE_PAGE_ALIGNMENT = 2097152;
            static const long HUGE_PAGE_ALIGNMENT = 1073741824;

            std::deque<T*> pages;
            long pageLength;
            long pagePosition;
            long pageAlignment;
            long pageTailIndex;
            T* pageTail;

            virtual void expand();
        public:
            explicit PagedVersatilePool(long pageSize);
            virtual ~PagedVersatilePool();
            
            PagedVersatilePool(const PagedVersatilePool<T>& other) = delete;
            PagedVersatilePool<T>& operator=(const PagedVersatilePool<T>& other) = delete;
            PagedVersatilePool(PagedVersatilePool<T>&& other);
            PagedVersatilePool<T>& operator=(PagedVersatilePool<T>&& other);

            virtual T* getNew();
            virtual void softReset();
    };

    template<class T>
    PagedVersatilePool<T>::PagedVersatilePool(long pageSize) :
        pages(), pageLength(pageSize / sizeof(T)), pagePosition(0), pageTailIndex(0)
    {
        int rc;

        if ((pageLength * sizeof(T)) >= HUGE_PAGE_ALIGNMENT) {
            pageAlignment = HUGE_PAGE_ALIGNMENT;
        } else if ((pageLength * sizeof(T)) >= LARGE_PAGE_ALIGNMENT) {
            pageAlignment = LARGE_PAGE_ALIGNMENT;
        } else {
            pageAlignment = SMALL_PAGE_ALIGNMENT;
        }
        
        rc = posix_memalign((void**) &pageTail, pageAlignment, pageLength * sizeof(T));
        if (rc != 0) {
            pagePosition = pageLength + 1;
            throw std::bad_alloc{};
        }

        rc = madvise(pageTail, pageLength * sizeof(T), MADV_HUGEPAGE | MADV_RANDOM);
        if (rc != 0) {
            pagePosition = pageLength + 1;
            free(pageTail);
            throw std::bad_alloc{};
        }

        pages.push_back(pageTail);
    }

    template<class T>
    PagedVersatilePool<T>::~PagedVersatilePool()
    {
        while (pages.size() > 0) {
            pageTail = pages.back();
            pages.pop_back();
            free(pageTail);
        }
    }

    template<class T>
    PagedVersatilePool<T>::PagedVersatilePool(PagedVersatilePool<T>&& other) : pages(std::move(other.pages)),
        pageLength(other.pageLength), pagePosition(other.pagePosition), pageAlignment(other.pageAlignment)
    {
        pageTail = other.pageTail;
        pageTailIndex = other.pageTailIndex;

        other.pages = std::deque<T*>();
        other.pagePosition = 0;
        other.pageTail = NULL;
        other.pageTailIndex = 0;
    }

    template<class T>
    PagedVersatilePool<T>& PagedVersatilePool<T>::operator=(PagedVersatilePool<T>&& other)
    {
        if (this == &other) {
            return *this;
        }

        while (pages.size() > 0) {
            pageTail = pages.back();
            pages.pop_back();
            free(pageTail);
        }

        pages = std::move(other.pages);
        pageLength = other.pageLength;
        pagePosition = other.pagePosition;
        pageAlignment = other.pageAlignment;
        pageTail = other.pageTail;
        pageTailIndex = other.pageTailIndex;

        other.pages = std::deque<T*>();
        other.pagePosition = 0;
        other.pageTail = NULL;
        other.pageTailIndex = 0;

        return *this;
    }

    template<class T>
    void PagedVersatilePool<T>::expand()
    {
        int rc;

        if ((pageTailIndex + 1) >= pages.size()) {
            rc = posix_memalign((void**) &pageTail, pageAlignment, pageLength * sizeof(T));
            if (rc != 0) {
                pagePosition = pageLength + 1;
                throw std::bad_alloc{};
            }

            rc = madvise(pageTail, pageLength * sizeof(T), MADV_HUGEPAGE | MADV_RANDOM);
            if (rc != 0) {
                pagePosition = pageLength + 1;
                free(pageTail);
                throw std::bad_alloc{};
            }

            pages.push_back(pageTail);
            pagePosition = 0;
            pageTailIndex++;
        } else {
            pageTail = pages.at(pageTailIndex);
            pagePosition = 0;
            pageTailIndex++;
        }
    }

    template<class T>
    T* PagedVersatilePool<T>::getNew()
    {
        if (pagePosition >= pageLength) {
            expand();
        }

        return &pageTail[pagePosition++];
    }

    template<class T>
    void PagedVersatilePool<T>::softReset()
    {
        pageTailIndex = 0;
        pageTail = pages.at(pageTailIndex);
        pagePosition = 0;
    }
}

#endif