#ifndef OUROBOROS_PAGED_LINKED_ELEMENT_H
#define OUROBOROS_PAGED_LINKED_ELEMENT_H

namespace ouroboros
{
    template<class T>
    struct PagedLinkedElement
    {
        T element;
        PagedLinkedElement* next;

        PagedLinkedElement(T element) : element(element), next(NULL) { }
        ~PagedLinkedElement() = default;

        PagedLinkedElement(const PagedLinkedElement<T>& other) = default;
        PagedLinkedElement<T>& operator=(const PagedLinkedElement<T>& other) = default;
        PagedLinkedElement(PagedLinkedElement<T>&& other) = default;
        PagedLinkedElement<T>& operator=(PagedLinkedElement<T>&& other) = default;
    };
}

#endif