#ifndef OUROBOROS_SEQCAPVECTOR_H
#define OUROBOROS_SEQCAPVECTOR_H

#include <vector>

#define OUROBOROS_SEQCAPVECTOR_DEFAULT_CAPACITY_STEP 1048576

namespace ouroboros
{
    template <class T>
    class SeqCapVector
    {
        protected:
            std::vector<T*>* vect;
            T* currentBlock;
            unsigned long pos;
            unsigned long offset;
            unsigned long capacityActual;
            unsigned long capacityStep;
        public:
            SeqCapVector();
            SeqCapVector(long capacityStep);
            virtual ~SeqCapVector();

            virtual void expand();
            virtual long push_back(T elem);
            virtual T& operator[](long idx);
    };

    template <class T>
    SeqCapVector<T>::SeqCapVector()
    {
        capacityStep = OUROBOROS_SEQCAPVECTOR_DEFAULT_CAPACITY_STEP;
        vect = new std::vector<T*>();
        capacityActual = 0;
        expand();
        pos = 0;
    }

    template <class T>
    SeqCapVector<T>::SeqCapVector(long capacityStep)
    {
        this->capacityStep = capacityStep;
        vect = new std::vector<T*>();
        capacityActual = 0;
        expand();
        pos = 0;
    }

    template <class T>
    SeqCapVector<T>::~SeqCapVector()
    {
        for (unsigned long i = 0; i < (capacityActual / capacityStep); i++) {
            delete[] (*vect)[i];
        }
        delete vect;
    }

    template <class T>
    void SeqCapVector<T>::expand()
    {
        currentBlock = new T[capacityStep]{};
        vect->push_back(currentBlock);
        capacityActual += capacityStep;
        offset = 0;
    }

    template <class T>
    long SeqCapVector<T>::push_back(T elem)
    {
        long idx = pos++;
        long j;

        if ((unsigned long) idx >= capacityActual) {
            expand();
        }
        j = offset++;

        currentBlock[j] = elem;

        return idx;
    }

    template <class T>
    T& SeqCapVector<T>::operator[](long idx)
    {
        long i = idx / capacityStep;
        long j = idx % capacityStep;

        return (*vect)[i][j];
    }
}

#endif