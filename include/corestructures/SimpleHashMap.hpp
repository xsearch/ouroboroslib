#ifndef OUROBOROS_SIMPLEHASHMAP_H
#define OUROBOROS_SIMPLEHASHMAP_H

#include <functional>

namespace ouroboros
{
    class SimpleHashMap
    {
        protected:
            long *buckets;
            unsigned long numBuckets;
            std::hash<char*> hashFn;
        public:
            SimpleHashMap(unsigned long numBuckets);
            virtual ~SimpleHashMap();

            virtual unsigned long hash(char* str);
            virtual void insert(char *key, long value);
            virtual long lookup(char *key);
    };
}

#endif