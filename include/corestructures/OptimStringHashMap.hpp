#ifndef OUROBOROS_OPTIMSTRINGHASHMAP_H
#define OUROBOROS_OPTIMSTRINGHASHMAP_H

#include "corestructures/SeqCapVector.hpp"

#include <vector>
#include <functional>
#include <cstring>

#define OUROBOROS_OPTIMSTRINGHASHMAP_EMPTY_BUCKET -1
#define OUROBOROS_OPTIMSTRINGHASHMAP_NOT_FOUND -2

namespace ouroboros
{
    class OptimStringHashMap
    {
        protected:
            struct chain_bucket_t
            {
                long* idxs;
                chain_bucket_t *next;
            };

            struct head_bucket_t
            {
                long idx;
            };

            head_bucket_t* buckets;
            SeqCapVector<char*>* keys;
            unsigned long numBuckets;
            unsigned long pos;
            std::hash<char*> hashFn;

        public:
            OptimStringHashMap(unsigned long numBuckets);
            virtual ~OptimStringHashMap();
            
            virtual unsigned long hash(char* str);
            virtual long insert(char* key);
            virtual long lookup(char* key);
    };
}

#endif