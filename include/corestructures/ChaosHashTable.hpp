#ifndef OUROBOROS_CHAOS_HASH_TABLE_H
#define OUROBOROS_CHAOS_HASH_TABLE_H

#include "corestructures/PagedVersatilePool.hpp"

#include <functional>
#include <limits>
#include <deque>

namespace ouroboros
{
    template<class K, class V>
    struct ChaosNode
    {
        unsigned long hashKey;
        ChaosNode* next;
        K key;
        V value;
    };

    template<class K, class V>
    struct ChaosBucket
    {
        unsigned long hashKey;
        ChaosNode<K, V>* next;
        K key;
        V value;
    };

    template<class K, class V, class Hash = std::hash<K>, class KeyEqual = std::equal_to<K>>
    class ChaosHashTable
    {
        private:
            PagedVersatilePool<ChaosNode<K, V>>* pool;
            ChaosBucket<K, V>* buckets;
            unsigned long numBuckets;
            unsigned long bktInterval;
            K emptyKey;
            Hash hashFn;
            KeyEqual equalFn;
        public:
            explicit ChaosHashTable(ChaosBucket<K, V>* buckets, unsigned long numBuckets, K emptyKey);
            virtual ~ChaosHashTable();

            ChaosHashTable(const ChaosHashTable& other) = delete;
            ChaosHashTable<K, V>& operator=(const ChaosHashTable<K, V>& other) = delete;
            ChaosHashTable(ChaosHashTable&& other);
            ChaosHashTable<K, V>& operator=(ChaosHashTable<K, V>&& other);

            virtual void insert(K key, V value);
            virtual bool lookup(K key, V& value);
            virtual ChaosBucket<K, V>* getBucket(unsigned long idx);
            virtual void softReset();
    };

    template<class K, class V, class Hash, class KeyEqual>
    ChaosHashTable<K, V, Hash, KeyEqual>::ChaosHashTable(ChaosBucket<K, V>* buckets,
                                                         unsigned long numBuckets,
                                                         K emptyKey)
    {
        this->buckets = buckets;
        this->numBuckets = numBuckets;
        this->bktInterval = std::numeric_limits<unsigned long>::max() / numBuckets;
        this->emptyKey = emptyKey;
        for (unsigned long i; i < numBuckets; i++) {
            buckets[i].key = emptyKey;
            buckets[i].next = NULL;
        }
        pool = new PagedVersatilePool<ChaosNode<K, V>>(numBuckets * sizeof(ChaosNode<K, V>));
    }

    template<class K, class V, class Hash, class KeyEqual>
    ChaosHashTable<K, V, Hash, KeyEqual>::~ChaosHashTable()
    {
        delete pool;
    }

    template<class K, class V, class Hash, class KeyEqual>
    ChaosHashTable<K, V, Hash, KeyEqual>::ChaosHashTable(ChaosHashTable&& other)
    {
        this->buckets = other.buckets;
        this->numBuckets = other.numBuckets;
        this->bktInterval = other.bktInterval;
        this->emptyKey = other.emptyKey;
        this->pool = other.pool;

        other.buckets = NULL;
        other.numBuckets = 0;
        other.pool = NULL;
    }

    template<class K, class V, class Hash, class KeyEqual>
    ChaosHashTable<K, V>& ChaosHashTable<K, V, Hash, KeyEqual>::operator=(ChaosHashTable<K, V>&& other)
    {
        if (this == &other) {
            return *this;
        }

        delete this->pool;

        this->buckets = other.buckets;
        this->numBuckets = other.numBuckets;
        this->bktInterval = other.bktInterval;
        this->emptyKey = other.emptyKey;
        this->pool = other.pool;

        other.buckets = NULL;
        other.numBuckets = 0;
        other.pool = NULL;

        return *this;
    }

    template<class K, class V, class Hash, class KeyEqual>
    void ChaosHashTable<K, V, Hash, KeyEqual>::insert(K key, V value)
    {
        unsigned long hashKey;
        unsigned long idx;
        ChaosNode<K, V>* node;

        hashKey = hashFn(key);
        idx = hashKey / bktInterval;

        if (buckets[idx].key == emptyKey) {
            buckets[idx].hashKey = hashKey;
            buckets[idx].key = key;
            buckets[idx].value = value;
            buckets[idx].next = NULL;
            return;
        }

        if (buckets[idx].next == NULL) {
            buckets[idx].next = pool->getNew();
            node = buckets[idx].next;
        } else {
            node = buckets[idx].next;
            while (node->next != NULL) {
                node = node->next;
            }
            node->next = pool->getNew();
            node = node->next;
        }

        node->hashKey = hashKey;
        node->key = key;
        node->value = value;
        node->next = NULL;
    }

    template<class K, class V, class Hash, class KeyEqual>
    bool ChaosHashTable<K, V, Hash, KeyEqual>::lookup(K key, V& value)
    {
        unsigned long hashKey;
        unsigned long idx;
        ChaosNode<K, V>* node;

        hashKey = hashFn(key);
        idx = hashKey / bktInterval;

        if (buckets[idx].key != emptyKey) {
            if ((buckets[idx].hashKey == hashKey) and equalFn(buckets[idx].key, key)) {
                value = buckets[idx].value;
                return true;
            } else {
                node = buckets[idx].next;
                while (node != NULL) {
                    if ((node->hashKey == hashKey) and equalFn(node->key, key)) {
                        value = node->value;
                        return true;
                    }
                    node = node->next;
                }
                return false;
            }
        } else {
            return false;
        }
    }

    template<class K, class V, class Hash, class KeyEqual>
    inline ChaosBucket<K, V>* ChaosHashTable<K, V, Hash, KeyEqual>::getBucket(unsigned long idx)
    {
        if (idx >= numBuckets) {
            return NULL;
        } else {
            return &(buckets[idx]);
        }
    }

    template<class K, class V, class Hash, class KeyEqual>
    inline void ChaosHashTable<K, V, Hash, KeyEqual>::softReset()
    {
        for (unsigned long i; i < numBuckets; i++) {
            buckets[i].key = emptyKey;
            buckets[i].next = NULL;
        }

        pool->softReset();
    }
}

#endif