#ifndef OUROBOROS_SIMPLEHASHMAPV4_H
#define OUROBOROS_SIMPLEHASHMAPV4_H

#include <functional>

namespace ouroboros
{
    class SimpleHashMapV4
    {
        protected:
            char* rawBuckets;
            unsigned long numBuckets;
            unsigned long bucketSize;
            std::hash<char*> hashFn;
        public:
            SimpleHashMapV4(unsigned long numBuckets, unsigned long bucketSize);
            virtual ~SimpleHashMapV4();

            virtual unsigned long hash(char* str);
            virtual void insert(char *key, long value);
            virtual long lookup(char *key);
    };
}

#endif