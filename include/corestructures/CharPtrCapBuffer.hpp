#ifndef OUROBOROS_CHARPTRCAPBUFFER_H
#define OUROBOROS_CHARPTRCAPBUFFER_H

#include <vector>
#include <cstring>

namespace ouroboros
{
    class CharPtrCapBuffer
    {
        protected:
            std::vector<char*> *vect;
            long capacity;
            long capacityStep;
            long pos;
            long length;
        public:
            CharPtrCapBuffer(long capacityStep);
            virtual ~CharPtrCapBuffer();

            virtual char* copyToPos(char *str);
            virtual void movePosNext();
    };

    CharPtrCapBuffer::CharPtrCapBuffer(long capacityStep)
    {
        this->capacityStep = capacityStep;
        vect = new std::vector<char*>();
        vect->push_back(new char[capacityStep]{0});
        capacity = capacityStep;
        pos = 0;
        length = -1;
    }

    CharPtrCapBuffer::~CharPtrCapBuffer()
    {
        for (auto buff : (*vect)) {
            delete[] buff;
        }
    }

    char* CharPtrCapBuffer::copyToPos(char *str)
    {
        long i, j;
        length = std::strlen(str) + 1;

        if ((pos + length) >= capacity) {
            vect->push_back(new char[capacityStep]{0});
            pos = capacity;
            capacity += capacityStep;
        }

        i = pos / capacityStep;
        j = pos % capacityStep;

        std::strcpy(&((*vect)[i][j]), str);

        return &((*vect)[i][j]);
    }

    void CharPtrCapBuffer::movePosNext()
    {
        pos += length;
    }
}

#endif