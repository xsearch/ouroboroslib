#ifndef OUROBOROS_HUGE_PAGED_BYTES_STORE_H
#define OUROBOROS_HUGE_PAGED_BYTES_STORE_H

#include <deque>

namespace ouroboros
{
    class HugePagedBytesStore
    {
        private:
            static const long SMALL_PAGE_ALIGNMENT = 4096;
            static const long LARGE_PAGE_ALIGNMENT = 2097152;
            static const long HUGE_PAGE_ALIGNMENT = 1073741824;

            std::deque<char*> pages;
            long pageSize;
            long pagePosition;
            long pageAlignment;
            char* pageTail;

            virtual void expand();
        public:
            explicit HugePagedBytesStore(long pageSize);
            virtual ~HugePagedBytesStore();

            // copy-constructor
            HugePagedBytesStore(const HugePagedBytesStore& other);
            // copy-assignment
            HugePagedBytesStore& operator=(const HugePagedBytesStore& other);
            // move-constructor
            HugePagedBytesStore(HugePagedBytesStore&& other);
            // move-assignment
            HugePagedBytesStore& operator=(HugePagedBytesStore&& other);
            
            /* method used to return a contigous range of bytes of size length*/
            virtual char* getBytes(long length);
    };
}

#endif