#include <iostream>
#include <cstring>
#include <thread>
#include <atomic>
#include <cstdio>

extern "C"
{
    #include <sys/time.h>
}

#include "ouroboros.hpp"
#include <vector>
#include <tuple>

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_termidxhrchy <total size in bytes> <term size in bytes> <number of threads>"
#define MAP_WIDTH 67108864
#define VECTOR_WIDTH 4194304

void work_index(long num_terms, vector<char*> *terms, BaseTermIndex *termIndex, int thread_id, int num_threads)
{
    for (long i = thread_id; i < num_terms; i += num_threads) {
        termIndex->insert(terms->at(i));
    }
}

int benchmark(long num_terms, vector<char*> *terms, int num_threads)
{
    BaseTermIndex *termIndex;
    vector<thread*> threads;
    int rc;

    termIndex = new HRCHYHashMapTermIndex(MAP_WIDTH, VECTOR_WIDTH);
    
    for (auto i = 0; i < num_threads; i++) {
        threads.push_back(new thread(work_index, num_terms, terms, termIndex, i, num_threads));
    }

    for (auto i = 0; i < num_threads; i++) {
        threads[i]->join();
    }
    
    rc = termIndex->lookup(terms->at(num_threads));

    delete termIndex;

    return rc;
}

int main(int argc, char **argv)
{
    long total_size;
    int term_size;
    int num_threads;
    long num_terms;
    
    char *buffer;
    vector<char*> *terms;
    tuple<std::vector<char*>*, char*> data;

    struct timeval start, end;
    double exec_time;
    int rc = 0;

    if (argc != 4) {
        cout << MSG << endl;
        return 1;
    }

    total_size = atol(argv[1]);
    term_size = atoi(argv[2]);
    num_threads = atoi(argv[3]);
    num_terms = total_size / (long) term_size;

    data = generate_synthdata(num_terms, term_size - 1);
    buffer = get<1>(data);
    terms = get<0>(data);
    
    gettimeofday(&start, NULL);
    rc = benchmark(num_terms, terms, num_threads);
    gettimeofday(&end, NULL);
    exec_time = (double) (end.tv_sec - start.tv_sec) + (double) (end.tv_usec - start.tv_usec) / 1000000;

    delete terms;
    delete[] buffer;

    cout << "Finished hashing " << total_size << " bytes of data with " << num_threads \
        << " thread(s) and a term size of " << term_size << " bytes!" << endl;
    cout << "Total execution time: " << exec_time << " seconds" << endl;
    cout << "Throughput: " << (total_size / (1024 * 1024)) / exec_time << " MB/sec" << endl;
    cout << "Throughput: " << ((double) num_terms / 1000) / exec_time << " kOPS" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}