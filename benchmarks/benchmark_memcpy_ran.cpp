#include <iostream>
#include <thread>
#include <chrono>
#include <tuple>
#include <random>
#include <cstring>

#include "ouroboros.hpp"

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_memcpy_ran <total size in bytes> <number of threads>"

#define MIN_ENTRY_SIZE 8
#define MAX_ENTRY_SIZE 32
#define BLOCK_SIZE 268435456 // 256MiB

void work_copy(char* buffer_in,
               char* buffer_out,
               long size,
               long* pattern_in,
               long* pattern_out,
               long pattern_size,
               int numa_id)
{
    NUMAConfig config;
    config.set_numa_node(numa_id);

    {
        long i, j, k, n;

        for (long b = 0; b < size; b += BLOCK_SIZE) {
            i = 0;
            j = 0;
            k = 0;
            while (k < pattern_size) {
                i = b + pattern_in[k];
                j = b + pattern_out[k];
                n = strlen(&buffer_in[i]) + 1;
                memcpy(&buffer_out[j], &buffer_in[i], n);
                k++;
            }
        }
    }
}

void work_init(char* buffer_in,
               char* buffer_out,
               long size,
               long* pattern_in,
               long pattern_size,
               long& num_entries,
               int numa_id)
{
    NUMAConfig config;
    config.set_numa_node(numa_id);

    {
        char characters[63] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        long i, j, k, n;

        i = 0;
        j = 0;
        k = 0;
        num_entries = 0;
        while (i < size) {
            n = pattern_in[(k + 1)] - pattern_in[k];
            memcpy(&buffer_in[i], &characters[j], n);
            buffer_in[i + n - 1] = 0;
            i += n;
            j = (j + 1) % (63 - MAX_ENTRY_SIZE);
            k = (k + 1) % pattern_size;
            num_entries++;
        }
    }
}

void work_mall(char*& buffer_in, char*& buffer_out, long size, int numa_id)
{
    NUMAConfig config;
    config.set_numa_node(numa_id);

    {
        buffer_in = new char[size]{'x'};
        buffer_out = new char[size + MAX_ENTRY_SIZE]{'x'};
    }
}

void generate_shuffle_pattern(long*& pattern_in,
                              long*& pattern_out,
                              long& pattern_size,
                              int numa_id,
                              unsigned long seed)
{
    NUMAConfig config;
    config.set_numa_node(numa_id);
    {
        long i, j, n;
        pattern_in = new long[BLOCK_SIZE / MIN_ENTRY_SIZE + 1]{};
        pattern_out = new long[BLOCK_SIZE / MIN_ENTRY_SIZE + 1]{};

        srand(seed);
        i = 0;
        j = 0;
        while (i < BLOCK_SIZE) {
            n = MIN_ENTRY_SIZE + (rand() % (MAX_ENTRY_SIZE - MIN_ENTRY_SIZE + 1));
            n = ((i + n) >= BLOCK_SIZE) ? (BLOCK_SIZE - i) : n;
            pattern_in[j] = i;
            pattern_out[j] = i;
            i += n;
            j++;
        }
        pattern_in[j] = BLOCK_SIZE;
        pattern_out[j] = BLOCK_SIZE;
        pattern_size = j;

        mt19937_64 random_number_generator(seed);
        uniform_int_distribution<long> distribution(0, pattern_size);

        for (i = 0; i < pattern_size; i++) {
            j = distribution(random_number_generator);
            exchange(pattern_out[i], pattern_out[j]);
        }
    }
}

tuple<double, double, double, long> benchmark(long total_size, int num_threads)
{
    char** buffers_in;
    char** buffers_out;
    long** patterns_in;
    long** patterns_out;
    long* patterns_size;
    long* num_entries;
    long size_per_thread;
    vector<thread> threads_mall;
    vector<thread> threads_gene;
    vector<thread> threads_init;
    vector<thread> threads_copy;
    int num_numa_nodes;
    NUMAConfig config;

    unsigned long seed = chrono::system_clock::now().time_since_epoch().count();
    chrono::time_point<chrono::high_resolution_clock> start, end;
    chrono::duration<double> diff;
    double mall_time(0.0), init_time(0.0), exec_time(0.0);
    long rc = -1;

    num_numa_nodes = config.get_num_numa_nodes();
    num_numa_nodes = (num_threads < num_numa_nodes) ? num_threads : num_numa_nodes;

    size_per_thread = total_size / num_threads;
    patterns_in = new long*[num_numa_nodes]{};
    patterns_out = new long*[num_numa_nodes]{};
    patterns_size = new long[num_numa_nodes]{};
    buffers_in = new char*[num_threads]{};
    buffers_out = new char*[num_threads]{};
    num_entries = new long[num_threads]{};

    for (int i = 0; i < num_numa_nodes; i++) {
        threads_gene.push_back(thread(generate_shuffle_pattern,
                                      ref(patterns_in[i]),
                                      ref(patterns_out[i]),
                                      ref(patterns_size[i]),
                                      i,
                                      seed));
    }

    for (int i = 0; i < num_numa_nodes; i++) {
        threads_gene[i].join();
    }

    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_threads; i++) {
        threads_mall.push_back(thread(work_mall,
                                      ref(buffers_in[i]),
                                      ref(buffers_out[i]),
                                      size_per_thread,
                                      i % num_numa_nodes));
    }

    for (int i = 0; i < num_threads; i++) {
        threads_mall[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    mall_time = diff.count();

    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_threads; i++) {
        threads_init.push_back(thread(work_init,
                                      buffers_in[i],
                                      buffers_out[i],
                                      size_per_thread,
                                      patterns_in[i % num_numa_nodes],
                                      patterns_size[i % num_numa_nodes],
                                      ref(num_entries[i]),
                                      i % num_numa_nodes));
    }

    for (int i = 0; i < num_threads; i++) {
        threads_init[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    init_time = diff.count();

    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_threads; i++) {
        threads_copy.push_back(thread(work_copy,
                                      buffers_in[i],
                                      buffers_out[i],
                                      size_per_thread,
                                      patterns_in[i % num_numa_nodes],
                                      patterns_out[i % num_numa_nodes],
                                      patterns_size[i % num_numa_nodes],
                                      i % num_numa_nodes));
    }

    for (int i = 0; i < num_threads; i++) {
        threads_copy[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    exec_time = diff.count();
    
    rc = 0;
    for (int i = 0; i < num_threads; i++) {
        rc += num_entries[i];
    }

    for (int i = 0; i < num_threads; i++) {
        delete[] buffers_in[i];
        delete[] buffers_out[i];
    }
    delete[] buffers_in;
    delete[] buffers_out;
    delete[] num_entries;

    for (int i = 0; i < num_numa_nodes; i++) {
        delete[] patterns_in[i];
        delete[] patterns_out[i];
    }

    delete[] patterns_in;
    delete[] patterns_out;
    delete[] patterns_size;

    return make_tuple(mall_time, init_time, exec_time, rc);
}

int main(int argc, char** argv)
{
    long total_size;
    int num_threads;

    tuple<double, double, double, long> benchmark_rc;
    double mall_time;
    double init_time;
    double exec_time;
    long rc;

    if (argc != 3) {
        cout << MSG << endl;
        return 1;
    }

    total_size = atol(argv[1]);
    num_threads = atol(argv[2]);

    benchmark_rc = benchmark(total_size, num_threads);
    mall_time = get<0>(benchmark_rc);
    init_time = get<1>(benchmark_rc);
    exec_time = get<2>(benchmark_rc);
    rc = get<3>(benchmark_rc);

    cout << "Finished copying " << total_size << " bytes of data:" << endl;
    cout << "- number of threads: " << num_threads << endl;
    cout << "- malloc and touch time: " << mall_time << " seconds" << endl;
    cout << "- initialization time: " << init_time << " seconds" << endl;
    cout << "- execution time: " << exec_time << " seconds" << endl;
    cout << "- throughput: " << (total_size / (1024 * 1024)) / exec_time << " MiB/sec" << endl;
    cout << "- throughput: " << rc / exec_time << " tokens/sec" << endl;
    cout << "Return code: " << rc << endl;
}