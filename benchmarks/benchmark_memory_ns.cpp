#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>
#include <random>
#include <functional>

#include "ouroboros.hpp"
#include <vector>
#include <tuple>

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_memory_ns <total number of words(int)> \
<block size in number of words(int)> <number of threads> <number of walks>"

struct worker_data
{
    long* pos_buffer;
    long* val_buffer;
    long* schema;
    long num_blocks;
    long block_num_words;
    int num_walks;
};

void worker_init(worker_data& data, int thread_id)
{
    NUMAConfig config;
    config.set_task_numa_node(thread_id);

    {
        long* schema = data.schema;
        long num_blocks = data.num_blocks;
        long block_num_words = data.block_num_words;
        long* pos_buffer = new long[num_blocks * block_num_words];
        long* val_buffer = new long[num_blocks * block_num_words];

        for (long i = 0; i < num_blocks; i++) {
            long offset = i * block_num_words;
            for (long j = 0; j < block_num_words; j++) {
                pos_buffer[offset + j] = schema[j] + offset;
                val_buffer[offset + j] = -1;
            }
        }

        data.pos_buffer = pos_buffer;
        data.val_buffer = val_buffer;
    }
}

void worker_work(worker_data& data, int thread_id)
{
    NUMAConfig config;
    config.set_task_numa_node(thread_id);

    {
        long* pos_buffer = data.pos_buffer;
        long* val_buffer = data.val_buffer;
        long num_blocks = data.num_blocks;
        long block_num_words = data.block_num_words;
        int num_walks = data.num_walks;

        for (int k = 0; k < num_walks; k++) {
            for (long i = 0; i < (num_blocks * block_num_words); i++) {
                val_buffer[pos_buffer[i]] = i;
            }
        }
    }
}

tuple<double, int> benchmark(long num_blocks, long block_num_words, int num_threads, int num_walks, long* schema)
{
    vector<thread> init_threads;
    vector<thread> work_threads;
    vector<worker_data> data_threads;

    chrono::time_point<chrono::high_resolution_clock> start, end;
    chrono::duration<double> diff;
    double exec_time;
    int rc = 0;
    
    for (int i = 0; i < num_threads; i++) {
        worker_data data = worker_data{};
        data.schema = schema;
        data.num_blocks = num_blocks / num_threads;
        data.block_num_words = block_num_words;
        data.num_walks = num_walks;
        data_threads.push_back(data);
    }

    for (int i = 0; i < num_threads; i++) {
        init_threads.push_back(thread(worker_init, ref(data_threads[i]), i));
    }

    for (int i = 0; i < num_threads; i++) {
        init_threads[i].join();
    }

    start = chrono::high_resolution_clock::now();
    for (int i = 0; i < num_threads; i++) {
        work_threads.push_back(thread(worker_work, ref(data_threads[i]), i));
    }

    for (int i = 0; i < num_threads; i++) {
        work_threads[i].join();
    }
    end = chrono::high_resolution_clock::now();
    diff = end - start;
    exec_time = diff.count();

    for (auto i = 0; i < num_threads; i++) {
        delete[] data_threads[i].pos_buffer;
        delete[] data_threads[i].val_buffer;
    }

    return make_tuple(exec_time, rc);
}

unsigned long shuffle_schema(long* schema, long block_num_words)
{
    unsigned long seed = chrono::system_clock::now().time_since_epoch().count();
    mt19937_64 random_number_generator(seed);
    uniform_int_distribution<long> distribution(0, block_num_words);

    for (long i = 0; i < block_num_words; i++) {
        schema[i] = i;
    }

    for (long i = 0; i < block_num_words; i++) {
        long j = distribution(random_number_generator);
        exchange(schema[i], schema[j]);
    }

    return seed;
}

int main(int argc, char **argv)
{
    long total_num_words;
    long block_num_words;
    int num_threads;
    int num_walks;
    long num_blocks;
    long total_bytes;
    
    long* schema;
    unsigned long seed;
    double exec_time;
    int rc;
    tuple<double, int> benchmark_rc;

    if (argc != 5) {
        cout << MSG << endl;
        return 1;
    }

    total_num_words = atol(argv[1]);
    block_num_words = atol(argv[2]);
    num_threads = atoi(argv[3]);
    num_walks = atoi(argv[4]);
    num_blocks = total_num_words / block_num_words;
    total_bytes = total_num_words * sizeof(long);

    schema = new long[block_num_words];
    seed = shuffle_schema(schema, block_num_words);
    
    benchmark_rc = benchmark(num_blocks, block_num_words, num_threads, num_walks, schema);
    exec_time = get<0>(benchmark_rc);
    rc = get<1>(benchmark_rc);

    delete[] schema;

    cout << "Finished memorizing!" << endl;
    cout << "- Total number of bytes: " << (total_bytes) << endl;
    cout << "- Number of walks: " << num_walks  << endl;
    cout << "- Number of threads: " << num_threads  << endl;
    cout << "- Block size (in bytes): " << (block_num_words * sizeof(long)) << endl;
    cout << "- Total execution time: " << exec_time << " seconds" << endl;
    cout << "- Throughput: " << ((num_walks * total_bytes) / (1024 * 1024)) / exec_time << " MiB/sec" << endl;
    cout << "- Throughput: " << ((num_walks * total_num_words) / (1000 * 1000)) / exec_time << " mOPS" << endl;
    cout << "- Seed: " << seed << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}