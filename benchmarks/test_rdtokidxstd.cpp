#include <iostream>
#include <cstring>
#include <thread>
#include <atomic>
#include <cstdio>
#include <cstdlib>
#include <vector>
#include <tuple>

extern "C"
{
    #include <sys/time.h>
}

#include "ouroboros.hpp"

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./test_rdtokidxstd <source file> <total size in bytes> \
<block size in bytes> <number of IO threads> <number of threads> <search term>"

#define QUEUE_SIZE 1024
#define DELIMS " \t\n/_.,;:"

void work_idx(BaseTermIndex *termIndex, BaseTermFileRelation *invertedIndex, CTokBLock *block, long fileIdx)
{
    long termIdx;
    long invertedIdx;

    for (auto i = 0; i < block->numTokens; ++i) {
        termIdx = termIndex->insert(block->tokens[i]);
        invertedIdx = invertedIndex->insert(termIdx, fileIdx);
    }
}

void work_tokidx(DualQueue<FileDataBlock*> *queue, 
                 BaseTermIndex *termIndex,
                 BaseTermFileRelation *invertedIndex,
                 int block_size)
{
    FileDataBlock *dataBlock;
    BranchLessTokenizer *tokenizer;
    CTokBLock *tokBlock;
    char *buffer;
    char **tokens;
    char delims[32] = DELIMS;
    int length;

    buffer = new char[block_size + 1];
    tokens = new char*[block_size / 2 + 1];
    tokBlock = new CTokBLock();
    tokBlock->buffer = buffer;
    tokBlock->tokens = tokens;
    tokenizer = new BranchLessTokenizer(delims);

    while (true) {
        dataBlock = queue->pop_full();
        
        length = dataBlock->length;
        if (length > 0) {
            tokenizer->getTokens(dataBlock, tokBlock);
            work_idx(termIndex, invertedIndex, tokBlock, dataBlock->fileIdx);
        }
        
        queue->push_empty(dataBlock);

        if (length == -1) {
            break;
        }
    }
}

void work_read(DualQueue<FileDataBlock*> *queue,
               BaseFileIndex *fileIndex,
               atomic<int> *position,
               vector<char*>filenames,
               int block_size)
{
    PFileReaderDriver *reader;
    FileDataBlock *dataBlock;
    int pos;
    long fileIdx;

    pos = position->fetch_add(1, std::memory_order_seq_cst);
    while (pos < (int) filenames.size()) {
        reader = new PFileReaderDriver(filenames[pos], block_size);
        try {
            reader->open();
        } catch (exception &e) {
            cout << "ERR: could not open file " << filenames[pos] << endl;
            delete reader;
            pos = position->fetch_add(1, std::memory_order_seq_cst);
            continue;    
        }

        fileIdx = fileIndex->lookup(filenames[pos]);
        if (fileIdx == INDEX_NEXISTS) {
            cout << "ERR: no index for file " << filenames[pos] << endl;
            continue;
        }

        while (true) {
            dataBlock = queue->pop_empty();

            reader->readNextBlock(dataBlock);
            dataBlock->fileIdx = fileIdx;

            queue->push_full(dataBlock);

            if (dataBlock->length == 0) {
                break;
            }
        }

        reader->close();
        delete reader;

        pos = position->fetch_add(1, std::memory_order_seq_cst);
    }
}

void read_filenames(const char *file_path, vector<char*> &filenames, BaseFileIndex *fileIndex)
{
    FILE *fp;
    char *buffer, *rc;

    fp = fopen(file_path, "r");
    if (fp == NULL) {
        cout << "ERR: cannot open file " << file_path << endl;
        return;
    }

    while (true) {
        buffer = new char[4096];
        
        rc = fgets(buffer, 4096, fp);
        if (rc == NULL) {
            delete buffer;
            break;
        }
        if (buffer[strlen(buffer) - 1] == '\n') {
            buffer[strlen(buffer) - 1] = '\0';
        }

        filenames.push_back(buffer);
        fileIndex->insert(buffer);
    }

    fclose(fp);
}

long benchmark(const char *file_path, int num_io_threads, int num_threads, int block_size, const char *search_term)
{
    FileDataBlock *dataBlocks;
    FileDataBlock *finalBlock;
    char *buffers;
    DualQueue<FileDataBlock*> queue(QUEUE_SIZE);
    BaseFileIndex *fileIndex;
    BaseTermIndex *termIndex;
    BaseTermFileRelation *invertedIndex;
    std::vector<std::tuple<long, long>> *search_result;
    long searchIdx, fileIdx;
    vector<thread*> threads_io;
    vector<thread*> threads_tokidx;
    vector<char*> filenames;
    atomic<int> position(0);
    long rc(0);

    // read and index the path of the files
    fileIndex = new StdHashMapFileIndex();
    read_filenames(file_path, filenames, fileIndex);

    // prepare the file readers and the tokenizers/indexers
    dataBlocks = new FileDataBlock[QUEUE_SIZE]();
    buffers = NULL;
    rc = posix_memalign((void**) &buffers, 512, QUEUE_SIZE * (long) block_size);
    if (rc != 0) {
        cout << "Could not allocate buffers in the benchmark!" << endl;
        delete[] dataBlocks;
        return rc;
    }

    termIndex = new StdHashMapTermIndex();
    invertedIndex = new StdHashMapInvertedIndex();

    for (auto i = 0; i < QUEUE_SIZE; i++) {
        dataBlocks[i].buffer = &buffers[i * block_size];
        queue.push_empty(&dataBlocks[i]);
    }
    
    for (auto i = 0; i < num_io_threads; i++) {
        threads_io.push_back(new thread(work_read, &queue, fileIndex, &position, filenames, block_size));
    }

    for (auto i = 0; i < num_threads; i++) {
        threads_tokidx.push_back(new thread(work_tokidx, &queue, termIndex, invertedIndex, block_size));
    }

    for (auto i = 0; i < num_io_threads; i++) {
        threads_io[i]->join();
    }

    for (auto i = 0; i < num_threads; i++) {
        finalBlock = queue.pop_empty();
        finalBlock->length = -1;
        queue.push_full(finalBlock);
    }
    
    for (auto i = 0; i < num_threads; i++) {
        threads_tokidx[i]->join();
    }
    
    // execute exact match data retrieval query
    cout << "Executing exact match data retrieval query with search term: " << search_term << endl;
    searchIdx = termIndex->lookup(search_term);
    search_result = invertedIndex->lookup(searchIdx);
    cout << "Query results (one per line):" << endl;
    for (auto inverted_it = search_result->begin(); inverted_it < search_result->end(); ++inverted_it)
    {
        fileIdx = get<0>(*inverted_it);
        cout << "() " << fileIndex->reverseLookup(fileIdx) << endl;
        rc++;
    }
    cout << endl;

    free(buffers);
    delete[] dataBlocks;

    while (filenames.size() > 0) {
        buffers = filenames.back();
        filenames.pop_back();
        delete[] buffers;
    }

    delete search_result;
    delete invertedIndex;
    delete termIndex;
    delete fileIndex;

    return rc;
}

int main(int argc, char **argv)
{
    char file_path[4096];
    long total_size;
    int block_size;
    int num_io_threads;
    int num_threads;
    char search_term[256];

    struct timeval start, end;
    double exec_time;
    long rc = 0;

    if (argc != 7) {
        cout << MSG << endl;
        return 1;
    }

    strcpy(file_path, argv[1]);
    total_size = atol(argv[2]);
    block_size = atoi(argv[3]);
    num_io_threads = atoi(argv[4]);
    num_threads = atoi(argv[5]);
    strcpy(search_term, argv[6]);

    gettimeofday(&start, NULL);
    rc = benchmark(file_path, num_io_threads, num_threads, block_size, search_term);
    gettimeofday(&end, NULL);
    exec_time = (double) (end.tv_sec - start.tv_sec) + (double) (end.tv_usec - start.tv_usec) / 1000000;

    cout << "Finished indexing and searching " << total_size << " bytes of data with " << num_io_threads \
        << " IO thread(s), " << num_threads << " threads(s) and a block size of " << block_size << " bytes!" << endl;
    cout << "Total execution time: " << exec_time << " seconds" << endl;
    cout << "Throughput: " << (total_size / (1024 * 1024)) / exec_time << " MB/sec" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}