#include <iostream>
#include <cstring>
#include <thread>
#include <atomic>
#include <cstdio>

extern "C"
{
    #include <sys/time.h>
}

#include "ouroboros.hpp"
#include <vector>
#include <tuple>

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_stringhashmap <total size in bytes> <term size in bytes> \
<number of threads> <number of buckets> <min collision> <max collision> <buffer capacity step>"

void work_index(long num_terms,
                vector<char*> *terms,
                StringHashMap *termIndex,
                CharPtrCapBuffer *buffer,
                int thread_id,
                int num_threads)
{
    char *token;
    std::tuple<long, bool> rc;

    for (long i = thread_id; i < num_terms; i += num_threads) {
        token = buffer->copyToPos(terms->at(i));
        rc = termIndex->insert(token);
        if (!get<1>(rc)) {
            buffer->movePosNext();
        }
        if (get<0>(rc) == MAX_CAPACITY_REACHED) {
            cout << "ERR: Reached maximum bucket capacity!" << endl;
            break;
        }
    }
}

int benchmark(long num_terms,
              vector<char*> *terms,
              int num_threads,
              long num_buckets,
              long min_collisions,
              long max_collisions,
              long buff_cap_step)
{
    StringHashMap *termIndex;
    vector<thread*> threads;
    vector<CharPtrCapBuffer*> buffers;
    int rc = 0;
    std::tuple<long, bool> query_res;
    char query_term[10] = "000000000";

    termIndex = new StringHashMap(num_buckets, min_collisions, max_collisions);
    
    for (auto i = 0; i < num_threads; i++) {
        buffers.push_back(new CharPtrCapBuffer(buff_cap_step));
        threads.push_back(new thread(work_index, num_terms, terms, termIndex, buffers[i], i, num_threads));
    }

    for (auto i = 0; i < num_threads; i++) {
        threads[i]->join();
    }
    
    query_res = termIndex->lookup((*terms)[num_threads]);
    if (!get<1>(query_res)) {
        cout << "WARN: term <" << (*terms)[num_threads] << "> was not found in the hashmap!" << endl;
    } else {
        rc = get<0>(query_res);
    }

    query_res = termIndex->lookup(query_term);
    if (get<1>(query_res)) {
        cout << "WARN: term <" << query_term << "> was found in the hashmap!" << endl;
    }

    delete termIndex;
    for (auto i = 0; i < num_threads; i++) {
        delete buffers[i];
    }

    return rc;
}

int main(int argc, char **argv)
{
    long total_size;
    int term_size;
    int num_threads;
    long num_buckets;
    int min_collisions;
    int max_collisions;
    long buff_cap_step;
    long num_terms;
    
    char *buffer;
    vector<char*> *terms;
    tuple<std::vector<char*>*, char*> data;

    struct timeval start, end;
    double exec_time;
    int rc = 0;

    if (argc != 8) {
        cout << MSG << endl;
        return 1;
    }

    total_size = atol(argv[1]);
    term_size = atoi(argv[2]);
    num_threads = atoi(argv[3]);
    num_buckets = atol(argv[4]);
    min_collisions = atoi(argv[5]);
    max_collisions = atoi(argv[6]);
    buff_cap_step = atol(argv[7]);
    num_terms = total_size / (long) term_size;

    data = generate_synthdata(num_terms, term_size - 1);
    buffer = get<1>(data);
    terms = get<0>(data);
    
    gettimeofday(&start, NULL);
    rc = benchmark(num_terms, terms, num_threads, num_buckets, min_collisions, max_collisions, buff_cap_step);
    gettimeofday(&end, NULL);
    exec_time = (double) (end.tv_sec - start.tv_sec) + (double) (end.tv_usec - start.tv_usec) / 1000000;

    delete terms;
    delete[] buffer;

    cout << "Finished hashing " << total_size << " bytes of data with " << num_threads \
        << " thread(s) and a term size of " << term_size << " bytes!" << endl;
    cout << "Total execution time: " << exec_time << " seconds" << endl;
    cout << "Throughput: " << (total_size / (1024 * 1024)) / exec_time << " MB/sec" << endl;
    cout << "Throughput: " << ((double) num_terms / 1000) / exec_time << " kOPS" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}