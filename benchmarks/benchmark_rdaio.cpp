#include <iostream>
#include <cstring>
#include <thread>
#include <atomic>
#include <cstdio>
#include <cstdlib>

extern "C"
{
    #include <sys/time.h>
}

#include "ouroboros.hpp"

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_rdaio <source file> <total size in bytes> \
<number of threads> <block size in bytes> <iodepth>"
#define QUEUE_SIZE 1024
#define NUM_TOKENIZERS 4

void work_tok(DualQueue<FileDataBlock*> *queue, atomic<long> *numTokens)
{
    FileDataBlock *dataBlock;
    long toks(0);
    int length;

    while (true) {
        dataBlock = queue->pop_full();
        
        length = dataBlock->length;
        if (length > 0) {
            toks++;
        }
        
        queue->push_empty(dataBlock);

        if (length == -1) {
            break;
        }
    }

    *numTokens += toks;
}

void work_read(DualQueue<FileDataBlock*> *queue,
               atomic<int> *position,
               vector<char*>filenames,
               int block_size,
               char *cache,
               int iodepth)
{
    AIOFileReaderDriver *reader;
    FileDataBlock *dataBlock;
    int pos;

    pos = position->fetch_add(1, std::memory_order_seq_cst);
    while (pos < (int) filenames.size()) {
        reader = new AIOFileReaderDriver(filenames[pos], block_size, cache, iodepth);
        try {
            reader->open();
        } catch (exception &e) {
            cout << "ERR: could not open file " << filenames[pos] << endl;
            delete reader;
            pos = position->fetch_add(1, std::memory_order_seq_cst);
            continue;    
        }

        while (true) {
            dataBlock = queue->pop_empty();
            
            reader->readNextBlock(dataBlock);
            if (dataBlock->length == 0) {
                break;
            }
            
            queue->push_full(dataBlock);
        }

        reader->close();
        delete reader;

        pos = position->fetch_add(1, std::memory_order_seq_cst);
    }
}

void read_filenames(char *file_path, vector<char*> &filenames)
{
    FILE *fp;
    char *buffer, *rc;

    fp = fopen(file_path, "r");
    if (fp == NULL) {
        cout << "ERR: cannot open file " << file_path << endl;
        return;
    }

    while (true) {
        buffer = new char[4096];
        
        rc = fgets(buffer, 4096, fp);
        if (rc == NULL) {
            delete buffer;
            break;
        }
        if (buffer[strlen(buffer) - 1] == '\n') {
            buffer[strlen(buffer) - 1] = '\0';
        }

        filenames.push_back(buffer);
    }

    fclose(fp);
}

int benchmark(char *file_path, int num_threads, int block_size, int iodepth)
{
    FileDataBlock *dataBlocks;
    FileDataBlock *finalBlock;
    char *buffers;
    char *caches;
    char *cache;
    long cacheCap;
    vector<char*> filenames;
    DualQueue<FileDataBlock*> queue(QUEUE_SIZE);
    vector<thread*> threads_read;
    vector<thread*> threads_tok;
    atomic<long> numTokens(0);
    atomic<int> position(0);
    int rc;
    
    read_filenames(file_path, filenames);

    dataBlocks = new FileDataBlock[QUEUE_SIZE]();
    rc = posix_memalign((void**) &buffers, 512, QUEUE_SIZE * (long) block_size);
    if (rc != 0) {
        cout << "Could not allocate buffers in the benchmark!" << endl;
        delete[] dataBlocks;
        return rc;
    }
    
    cacheCap = (long) iodepth * (long) block_size;
    rc = posix_memalign((void**) &caches, 512, (long) num_threads * cacheCap);
    if (rc != 0) {
        cout << "Could not allocate buffers in the benchmark!" << endl;
        delete[] dataBlocks;
        free(buffers);
        return rc;
    }

    for (auto i = 0; i < QUEUE_SIZE; i++) {
        dataBlocks[i].buffer = &buffers[i * block_size];
        queue.push_empty(&dataBlocks[i]);
    }
    
    for (auto i = 0; i < num_threads; i++) {
        cache = &caches[i * cacheCap];
        threads_read.push_back(new thread(work_read, &queue, &position, filenames, block_size, cache, iodepth));
    }

    for (auto i = 0; i < NUM_TOKENIZERS; i++) {
        threads_tok.push_back(new thread(work_tok, &queue, &numTokens));
    }

    for (auto i = 0; i < num_threads; i++) {
        threads_read[i]->join();
    }

    for (auto i = 0; i < NUM_TOKENIZERS; i++) {
        finalBlock = queue.pop_empty();
        finalBlock->length = -1;
        queue.push_full(finalBlock);
    }
    
    for (auto i = 0; i < NUM_TOKENIZERS; i++) {
        threads_tok[i]->join();
    }
    
    rc = numTokens;

    delete[] buffers;
    delete[] caches;
    delete[] dataBlocks;

    while (filenames.size() > 0) {
        buffers = filenames.back();
        filenames.pop_back();
        delete[] buffers;
    }

    return rc;
}

int main(int argc, char **argv)
{
    char file_path[4096];
    long total_size;
    int num_threads;
    int block_size;
    int iodepth;
    struct timeval start, end;
    double exec_time;
    int rc = 0;

    if (argc != 6) {
        cout << MSG << endl;
        return 1;
    }

    strcpy(file_path, argv[1]);
    total_size = atol(argv[2]);
    num_threads = atoi(argv[3]);
    block_size = atoi(argv[4]);
    iodepth = atoi(argv[5]);

    gettimeofday(&start, NULL);
    rc = benchmark(file_path, num_threads, block_size, iodepth);
    gettimeofday(&end, NULL);
    exec_time = (double) (end.tv_sec - start.tv_sec) + (double) (end.tv_usec - start.tv_usec) / 1000000;

    cout << "Finished loading " << total_size << " bytes of data with " << num_threads \
         << " thread(s), a depth of " << iodepth << " and a block size of " << block_size << " bytes!" << endl;
    cout << "Total execution time: " << exec_time << " seconds" << endl;
    cout << "Throughput: " << (total_size / (1024 * 1024)) / exec_time << " MB/sec" << endl;
    cout << "Return code: " << rc << endl;


    return 0;
}