#include <iostream>
#include <cstring>
#include <thread>
#include <atomic>
#include <cstdio>

extern "C"
{
    #include <sys/time.h>
}

#include "ouroboros.hpp"
#include <vector>
#include <tuple>
#include <functional>

using namespace std;
using namespace ouroboros;

#define MSG "Usage: ./benchmark_stdhashing <total size in bytes> <term size in bytes> <number of threads>"

#define NUM_BUCKETS (((size_t)1 << 33) - 1)

void work_hash(long num_terms,
               vector<char*> *terms,
               vector<size_t> *hashes,
               hash<char*> *termHash,
               int thread_id,
               int num_threads)
{
    long N = num_terms / num_threads;

    for (long i = thread_id * N; i < (thread_id + 1) * N; i++) {
        (*hashes)[i] = (*termHash)(terms->at(i)) & NUM_BUCKETS;
    }
}

int benchmark(long num_terms, vector<char*> *terms, vector<size_t> *hashes, int num_threads)
{
    vector<hash<char*>*> termHashes;
    vector<thread*> threads;
    int rc = 0;
    
    for (auto i = 0; i < num_threads; i++) {
        termHashes.push_back(new hash<char*>());
        threads.push_back(new thread(work_hash, num_terms, terms, hashes, termHashes[i], i, num_threads));
    }

    for (auto i = 0; i < num_threads; i++) {
        threads[i]->join();
    }

    for (auto i = 0; i < num_threads; i++) {
        delete termHashes[i];
    }

    return rc;
}

int main(int argc, char **argv)
{
    long total_size;
    int term_size;
    int num_threads;
    long num_terms;
    
    char *buffer;
    vector<char*> *terms;
    vector<size_t> *hashes;
    tuple<std::vector<char*>*, char*, vector<size_t>*> data;

    struct timeval start, end;
    double exec_time;
    int rc = 0;

    if (argc != 4) {
        cout << MSG << endl;
        return 1;
    }

    total_size = atol(argv[1]);
    term_size = atoi(argv[2]);
    num_threads = atoi(argv[3]);
    num_terms = total_size / (long) term_size;

    data = generate_synthdatahash(num_terms, term_size - 1);
    buffer = get<1>(data);
    terms = get<0>(data);
    hashes = get<2>(data);
    
    gettimeofday(&start, NULL);
    rc = benchmark(num_terms, terms, hashes, num_threads);
    gettimeofday(&end, NULL);
    exec_time = (double) (end.tv_sec - start.tv_sec) + (double) (end.tv_usec - start.tv_usec) / 1000000;

    delete terms;
    delete hashes;
    delete[] buffer;

    cout << "Finished hashing " << total_size << " bytes of data with " << num_threads \
        << " thread(s) and a term size of " << term_size << " bytes!" << endl;
    cout << "Total execution time: " << exec_time << " seconds" << endl;
    cout << "Throughput: " << (total_size / (1024 * 1024)) / exec_time << " MB/sec" << endl;
    cout << "Throughput: " << ((double) num_terms / 1000) / exec_time << " kOPS" << endl;
    cout << "Return code: " << rc << endl;

    return 0;
}