**Illinois Institute of Technology**  
**Data-Intensive Distributed Systems Laboratory**  
**Project Omega - Ouroboros Library**  

A framework/library for building scalable high-performance search engines (HPSE).

# System Requirements

Operating systems: Ubuntu 18.04LTS, Ubuntu 20.04LTS (other Linux distros should technically work, but the library 
has been tested only on these operating systems);

Compilers: g++-8 or g++-9 (other compilers should technically work but they need to implement C++17 standard);

Main memory: for realistic workloads the library requires at least 8GiB of RAM;

# Dependencies

APIs: Linux low level IO (**open**, **close**, **pread**, **pwrite**), POSIX memory management (**posix_memalign**), 
Linux memory management (**madvise**), libnuma (development distro package, **numa_available**, **numa_max_node**, 
**numa_run_on_node**, **numa_set_preferred**);

Optional: google_dense_map (https://github.com/sparsehash/sparsehash 2.0.4), 
flat_hash_map (https://github.com/abseil/abseil-cpp lts_2021_03_24);

# How to build

On Ubuntu 18.04LTS install cmake, g++-8, make, libnuma-dev:
```
sudo apt install g++-8 make cmake libnuma-dev
```

If the g++ default compiler is set to a version lower than 8, you will need to configure g++8 as the default compiler:
```
sudo update-alternatives --remove-all g++
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-5 10
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-6 20
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-7 30
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-8 40
```

On Ubuntu 20.04LTS install cmake, g++-9, make, libnuma-dev:
```
sudo apt install g++-9 make cmake libnuma-dev
```

Clone this repository:
```
git clone https://gitlab.com/xsearch/ouroboroslib.git
```

**(Optional)** clone the google_dense_map and "Swiss Table" repositories:
```
cd ouroboroslib
git clone https://github.com/sparsehash/sparsehash.git
cd sparsehash
git checkout sparsehash-2.0.4
cd ..
git clone https://github.com/abseil/abseil-cpp.git
cd abseil-cpp
git checkout 20210324.2
cd ../..
```
Build the ouroboroslib project:
```
cd ouroboroslib
mkdir build
cd build
cmake ..
make
cd ../..
```

# How to run benchmarks

All of the benchmarks that are packaged with the library will be compiled under **build**. For example to run the 
benchmark_rd program:
```
cd ouroboroslib/build
./benchmark_rd [<arguments>]
```
