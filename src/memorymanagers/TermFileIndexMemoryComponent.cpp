#include "memorymanagers/TermFileIndexMemoryComponent.hpp"
#include "corestructures/HugePagedStringStore.hpp"
#include "indexing/knowledge/StdMapTermFileIndex.hpp"
#include "indexing/knowledge/DenseMapTermFileIndex.hpp"
#include "indexing/knowledge/SwissMapTermFileIndex.hpp"
#include "indexing/knowledge/ChainedTableTermFileIndex.hpp"
#include "indexing/knowledge/PagedStdMapTermFileIndex.hpp"
#include "indexing/knowledge/PagedDenseMapTermFileIndex.hpp"
#include "indexing/knowledge/PagedSwissMapTermFileIndex.hpp"
#include "indexing/knowledge/PagedChainedTableTermFileIndex.hpp"
#include <iostream>

namespace ouroboros
{
    TermFileIndexMemoryComponent::TermFileIndexMemoryComponent(long pageSize,
                                                               TermFileIndexMemoryComponentType type,
                                                               unsigned long numBuckets) : 
        BaseMemoryComponent(MemoryComponentType::TERM_FILE_INDEX)
    {
        termStore = std::shared_ptr<BasePagedStringStore>(new HugePagedStringStore(pageSize));
        switch (type) {
            case TermFileIndexMemoryComponentType::STD:
                index = std::shared_ptr<BaseTermFileIndex>(new StdMapTermFileIndex(termStore));
                break;
            case TermFileIndexMemoryComponentType::PSTD:
                indexStore = std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<char*, long>>>(
                    new PagedVersatileStore<PagedVersatileIndex<char*, long>>(pageSize / 2)
                );
                listStore = std::shared_ptr<PagedVersatileStore<PagedLinkedElement<long>>>(
                    new PagedVersatileStore<PagedLinkedElement<long>>(pageSize / 2)
                );
                index = std::shared_ptr<BaseTermFileIndex>(
                    new PagedStdMapTermFileIndex(termStore, indexStore, listStore)
                );
                break;
#ifdef GOOGLE_DENSE_LIB
            case TermFileIndexMemoryComponentType::DENSE:
                index = std::shared_ptr<BaseTermFileIndex>(new DenseMapTermFileIndex(termStore));
                break;
            case TermFileIndexMemoryComponentType::PDENSE:
                indexStore = std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<char*, long>>>(
                    new PagedVersatileStore<PagedVersatileIndex<char*, long>>(pageSize / 2)
                );
                listStore = std::shared_ptr<PagedVersatileStore<PagedLinkedElement<long>>>(
                    new PagedVersatileStore<PagedLinkedElement<long>>(pageSize / 2)
                );
                index = std::shared_ptr<BaseTermFileIndex>(
                    new PagedDenseMapTermFileIndex(termStore, indexStore, listStore)
                );
                break;
#endif
#ifdef GOOGLE_SWISS_LIB
            case TermFileIndexMemoryComponentType::SWISS:
                index = std::shared_ptr<BaseTermFileIndex>(new SwissMapTermFileIndex(termStore));
                break;
            case TermFileIndexMemoryComponentType::PSWISS:
                indexStore = std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<char*, long>>>(
                    new PagedVersatileStore<PagedVersatileIndex<char*, long>>(pageSize / 2)
                );
                listStore = std::shared_ptr<PagedVersatileStore<PagedLinkedElement<long>>>(
                    new PagedVersatileStore<PagedLinkedElement<long>>(pageSize / 2)
                );
                index = std::shared_ptr<BaseTermFileIndex>(
                    new PagedSwissMapTermFileIndex(termStore, indexStore, listStore)
                );
                break;
#endif
            case TermFileIndexMemoryComponentType::CHAINED:
                index = std::shared_ptr<BaseTermFileIndex>(new ChainedTableTermFileIndex(termStore, numBuckets));
                break;
            case TermFileIndexMemoryComponentType::PCHAINED:
                indexStore = std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<char*, long>>>(
                    new PagedVersatileStore<PagedVersatileIndex<char*, long>>(pageSize / 2)
                );
                listStore = std::shared_ptr<PagedVersatileStore<PagedLinkedElement<long>>>(
                    new PagedVersatileStore<PagedLinkedElement<long>>(pageSize / 2)
                );
                index = std::shared_ptr<BaseTermFileIndex>(
                    new PagedChainedTableTermFileIndex(termStore, indexStore, listStore, numBuckets)
                );
                break;
            default:
                std::cout << "WARN: picking default (STD) TermFileIndexMemoryComponent!" << std::endl;
                index = std::shared_ptr<BaseTermFileIndex>(new StdMapTermFileIndex(termStore));
        };
    }

    std::shared_ptr<BasePagedStringStore> TermFileIndexMemoryComponent::getPagedStringStore()
    {
        return termStore;
    }

    std::shared_ptr<BaseTermFileIndex> TermFileIndexMemoryComponent::getTermFileIndex()
    {
        return index;
    }
}