#include "memorymanagers/FileIndexMemoryComponent.hpp"
#include "indexing/sources/StdMapFileIndex.hpp"
#include <iostream>

namespace ouroboros
{
    FileIndexMemoryComponent::FileIndexMemoryComponent(FileIndexMemoryComponentType type,
                                                       unsigned long numBuckets) : 
        BaseMemoryComponent(MemoryComponentType::FILE_INDEX)
    {
        switch (type) {
            case FileIndexMemoryComponentType::STD:
                index = std::shared_ptr<BaseFileIndex>(new StdMapFileIndex());
                break;
            default:
                std::cout << "WARN: picking default (STD) FileIndexMemoryComponent!" << std::endl;
                index = std::shared_ptr<BaseFileIndex>(new StdMapFileIndex());
        };
    }

    std::shared_ptr<BaseFileIndex> FileIndexMemoryComponent::getFileIndex()
    {
        return index;
    }
}