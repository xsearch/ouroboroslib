#include "memorymanagers/TFIDFIndexMemoryComponent.hpp"
//#include "indexing/information/StdMapTFIDFIndex.hpp"
//#include "indexing/information/DenseMapTFIDFIndex.hpp"
//#include "indexing/information/SwissMapTFIDFIndex.hpp"
//#include "indexing/information/ChainedTableTFIDFIndex.hpp"
#include "indexing/information/PagedStdMapTFIDFIndex.hpp"
#include "indexing/information/PagedDenseMapTFIDFIndex.hpp"
#include "indexing/information/PagedSwissMapTFIDFIndex.hpp"
#include "indexing/information/PagedChainedTableTFIDFIndex.hpp"
#include <iostream>

namespace ouroboros
{
    TFIDFIndexMemoryComponent::TFIDFIndexMemoryComponent(long pageSize,
                                                         TFIDFIndexMemoryComponentType type,
                                                         unsigned long numBuckets) : 
        BaseMemoryComponent(MemoryComponentType::TFIDF_INDEX)
    {
        bytesStore = std::shared_ptr<HugePagedBytesStore>(new HugePagedBytesStore(pageSize));
        switch (type) {
            /*case TFIDFIndexMemoryComponentType::STD:
                index = std::shared_ptr<BaseTFIDFIndex>(new StdMapTFIDFIndex(termStore));
                break;*/
            case TFIDFIndexMemoryComponentType::PSTD:
                index = std::shared_ptr<BaseTFIDFIndex>(new PagedStdMapTFIDFIndex(bytesStore));
                break;
#ifdef GOOGLE_DENSE_LIB
            /*case TFIDFIndexMemoryComponentType::DENSE:
                index = std::shared_ptr<BaseTFIDFIndex>(new DenseMapTFIDFIndex(termStore));
                break;*/
            case TFIDFIndexMemoryComponentType::PDENSE:
                index = std::shared_ptr<BaseTFIDFIndex>(new PagedDenseMapTFIDFIndex(bytesStore));
                break;
#endif
#ifdef GOOGLE_SWISS_LIB
            /*case TFIDFIndexMemoryComponentType::SWISS:
                index = std::shared_ptr<BaseTFIDFIndex>(new SwissMapTFIDFIndex(termStore));
                break;*/
            case TFIDFIndexMemoryComponentType::PSWISS:
                index = std::shared_ptr<BaseTFIDFIndex>(new PagedSwissMapTFIDFIndex(bytesStore));
                break;
#endif
            /*case TFIDFIndexMemoryComponentType::CHAINED:
                index = std::shared_ptr<BaseTFIDFIndex>(new ChainedTableTFIDFIndex(termStore, numBuckets));
                break;*/
            case TFIDFIndexMemoryComponentType::PCHAINED:
                index = std::shared_ptr<BaseTFIDFIndex>(new PagedChainedTableTFIDFIndex(bytesStore, numBuckets));
                break;
            /*default:
                std::cout << "WARN: picking default (PSTD) TFIDFIndexMemoryComponent!" << std::endl;
                indexStore = std::shared_ptr<PagedVersatileStore<PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>>>(
                    new PagedVersatileStore<PagedVersatileIndex<TFIndexEntry, IDFIndexEntry>>(pageSize)
                );
                listStore = std::shared_ptr<PagedVersatileStore<PagedLinkedElement<IDFIndexEntry>>>(
                    new PagedVersatileStore<PagedLinkedElement<IDFIndexEntry>>(pageSize)
                );
                index = std::shared_ptr<BaseTFIDFIndex>(
                    new PagedStdMapTFIDFIndex(termStore, indexStore, listStore)
                );
                break;*/
        };
    }

    std::shared_ptr<HugePagedBytesStore> TFIDFIndexMemoryComponent::getPagedBytesStore()
    {
        return bytesStore;
    }

    std::shared_ptr<BaseTFIDFIndex> TFIDFIndexMemoryComponent::getTFIDFIndex()
    {
        return index;
    }
}