#include "memorymanagers/ChaosBucketsMemoryComponent.hpp"

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <new>

namespace ouroboros
{
    ChaosBucketsMemoryComponent::ChaosBucketsMemoryComponent(long numBuckets) : 
            BaseMemoryComponent(MemoryComponentType::CHAOS_BUCKETS)
    {
        int rc;

        this->numBuckets = numBuckets;

        if ((numBuckets * sizeof(ChaosBucket<char*, long>)) >= HUGE_PAGE_ALIGNMENT) {
            alignment = HUGE_PAGE_ALIGNMENT;
        } else if ((numBuckets * sizeof(ChaosBucket<char*, long>)) >= LARGE_PAGE_ALIGNMENT) {
            alignment = LARGE_PAGE_ALIGNMENT;
        } else {
            alignment = SMALL_PAGE_ALIGNMENT;
        }
        
        rc = posix_memalign((void**) &buckets, alignment, numBuckets * sizeof(ChaosBucket<char*, long>));
        if (rc != 0) {
            throw std::bad_alloc{};
        }

        rc = madvise(buckets, numBuckets * sizeof(ChaosBucket<char*, long>), MADV_HUGEPAGE | MADV_RANDOM);
        if (rc != 0) {
            throw std::bad_alloc{};
        }
    }
    
    ChaosBucketsMemoryComponent::~ChaosBucketsMemoryComponent()
    {
        free(buckets);
    }

    ChaosBucket<char*, long>* ChaosBucketsMemoryComponent::getChaosBuckets()
    {
        return buckets;
    }

    void ChaosBucketsMemoryComponent::configRandomAccess()
    {
        int rc;

        rc = madvise(buckets, numBuckets * sizeof(ChaosBucket<char*, long>), MADV_HUGEPAGE | MADV_RANDOM);
        if (rc != 0) {
            std::cout << "WARN: could not reconfigure memory to random!" << std::endl;
        }
    }

    void ChaosBucketsMemoryComponent::configSequentialAccess()
    {
        int rc;

        rc = madvise(buckets, numBuckets * sizeof(ChaosBucket<char*, long>), MADV_HUGEPAGE | MADV_SEQUENTIAL);
        if (rc != 0) {
            std::cout << "WARN: could not reconfigure memory to sequential!" << std::endl;
        }
    }
}
