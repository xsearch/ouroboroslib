#include "memorymanagers/ChaosHashTableMemoryComponent.hpp"

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <new>

namespace ouroboros
{
    ChaosHashTableMemoryComponent::ChaosHashTableMemoryComponent(ChaosBucket<char*, long>* buckets, long numBuckets) : 
            BaseMemoryComponent(MemoryComponentType::CHAOS_HASH_TABLE)
    {
        this->buckets = buckets;
        this->numBuckets = numBuckets;
        table = new ChaosHashTable<char*, long, cstr_hash, cstr_equal>(buckets, numBuckets, NULL);
    }
    
    ChaosHashTableMemoryComponent::~ChaosHashTableMemoryComponent()
    {
        free(table);
    }

    ChaosHashTable<char*, long, cstr_hash, cstr_equal>* ChaosHashTableMemoryComponent::getChaosHashTable()
    {
        return table;
    }
}
