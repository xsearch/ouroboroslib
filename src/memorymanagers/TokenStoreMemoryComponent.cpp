#include "memorymanagers/TokenStoreMemoryComponent.hpp"
#include "corestructures/HugePagedStringStore.hpp"

namespace ouroboros
{
    TokenStoreMemoryComponent::TokenStoreMemoryComponent(long pageSize):
            BaseMemoryComponent(MemoryComponentType::TOKEN_STORE)
    {
        store = new HugePagedStringStore(pageSize);
        tokens = new std::deque<char*>();
    }

    TokenStoreMemoryComponent::~TokenStoreMemoryComponent()
    {
        delete tokens;
        delete store;
    }

    BasePagedStringStore* TokenStoreMemoryComponent::getPagedStringStore()
    {
        return store;
    }

    std::deque<char*>* TokenStoreMemoryComponent::getTokens()
    {
        return tokens;
    }
}