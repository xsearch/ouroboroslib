#include "memorymanagers/FileDualQueueMemoryComponent.hpp"

#include <cstdlib>
#include <cstring>
#include <new>

namespace ouroboros
{
    FileDualQueueMemoryComponent::FileDualQueueMemoryComponent(int queueSize, int blockSize) : 
            BaseMemoryComponent(MemoryComponentType::DUALQUEUE)
    {
        int rc;

        this->queueSize = queueSize;
        rc = posix_memalign((void**) &buffer, DISK_ALIGNMENT, (long) queueSize * (long) blockSize);
        if (rc != 0) {
            dualQueue = nullptr;
            allocated = false;
            throw std::bad_alloc{};
        } else {
            std::memset(buffer, 'x', (long) queueSize * (long) blockSize);
            dataBlocks = new FileDataBlock[queueSize]();
            dualQueue = new DualQueue<FileDataBlock*>(queueSize);
            for (auto i = 0; i < queueSize; i++) {
                dataBlocks[i].buffer = &buffer[i * blockSize];
                dualQueue->push_empty(&dataBlocks[i]);
            }
            allocated = true;
        }
    }
    
    FileDualQueueMemoryComponent::~FileDualQueueMemoryComponent()
    {
        if (allocated) {
            free(buffer);
            delete[] dataBlocks;
            delete dualQueue;
        }
    }

    DualQueue<FileDataBlock*>* FileDualQueueMemoryComponent::getDualQueue()
    {
        return dualQueue;
    }

    bool FileDualQueueMemoryComponent::isAllocated() {
        return allocated;
    }
}
