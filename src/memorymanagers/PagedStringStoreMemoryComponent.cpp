#include "memorymanagers/PagedStringStoreMemoryComponent.hpp"
#include "corestructures/DefaultPagedStringStore.hpp"
#include "corestructures/HugePagedStringStore.hpp"
#include "corestructures/RandomPagedStringStore.hpp"

namespace ouroboros
{
    PagedStringStoreMemoryComponent::PagedStringStoreMemoryComponent(long pageSize, PagedStringStoreType type):
            BaseMemoryComponent(MemoryComponentType::PAGEDSTRINGSTORE)
    {
        switch (type) {
            case PagedStringStoreType::DEFAULT:
                store = new DefaultPagedStringStore(pageSize);
                break;
            case PagedStringStoreType::HUGE:
                store = new HugePagedStringStore(pageSize);
                break;
            case PagedStringStoreType::RANDOM:
                store = new RandomPagedStringStore(pageSize);
                break;
            default:
                store = new DefaultPagedStringStore(pageSize);
        }
    }

    PagedStringStoreMemoryComponent::~PagedStringStoreMemoryComponent()
    {
        delete store;
    }

    BasePagedStringStore* PagedStringStoreMemoryComponent::getPagedStringStore()
    {
        return store;
    }
}