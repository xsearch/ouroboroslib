#include "tokenizers/CTokBlock.hpp"

namespace ouroboros
{
    CTokBLock::CTokBLock(): BaseTokBlock(TokBlockType::C) {
        buffer = nullptr;
        tokens = nullptr;
        numTokens = 0;
    }
}