#include "tokenizers/StandardTokenizer.hpp"

#include <cstring>

namespace ouroboros
{
    StandardTokenizer::StandardTokenizer(char *delim): BaseTokenizer(TokenizerType::STANDARD) {
        this->delim = delim;
    }

    inline void StandardTokenizer::getTokens(BaseDataBlock *dataBlock, BaseTokBlock *tokBlock) {
        getTokens((FileDataBlock*) dataBlock, (CTokBLock*) tokBlock);
    }

    void StandardTokenizer::getTokens(FileDataBlock *dataBlock, CTokBLock *tokBlock) {
        char *token, *next_token;

        std::memcpy(tokBlock->buffer, dataBlock->buffer, dataBlock->blockSize);
        tokBlock->buffer[dataBlock->length] = '\0';

        tokBlock->numTokens = 0;
        token = strtok_r(tokBlock->buffer, delim, &next_token);
        while (token != NULL) {
            tokBlock->tokens[tokBlock->numTokens] = token;
            tokBlock->numTokens++;
            token = strtok_r(NULL, delim, &next_token);
        }
    }
}