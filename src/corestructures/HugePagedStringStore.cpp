#include "corestructures/HugePagedStringStore.hpp"

#include <cstring>
#include <cstdlib>
#include <new>

extern "C"
{
    #include <sys/mman.h>
}

namespace ouroboros
{
    HugePagedStringStore::HugePagedStringStore(long pageSize) : 
        BasePagedStringStore(PagedStringStoreType::HUGE), pages(), pageSize(pageSize), pagePosition(0)
    {
        int rc;

        if (pageSize >= HUGE_PAGE_ALIGNMENT) {
            pageAlignment = HUGE_PAGE_ALIGNMENT;
        } else if (pageSize >= LARGE_PAGE_ALIGNMENT) {
            pageAlignment = LARGE_PAGE_ALIGNMENT;
        } else {
            pageAlignment = SMALL_PAGE_ALIGNMENT;
        }
        
        rc = posix_memalign((void**) &pageTail, pageAlignment, pageSize);
        if (rc != 0) {
            pagePosition = pageSize + 1;
            throw std::bad_alloc{};
        }

        rc = madvise(pageTail, pageSize, MADV_HUGEPAGE);
        if (rc != 0) {
            pagePosition = pageSize + 1;
            free(pageTail);
            throw std::bad_alloc{};
        }
        
        std::memset(pageTail, 'x', pageSize);
        pages.push_back(pageTail);
    }

    HugePagedStringStore::~HugePagedStringStore()
    {
        while(pages.size() > 0) {
            pageTail = pages.back();
            pages.pop_back();
            free(pageTail);
        }
    }

    HugePagedStringStore::HugePagedStringStore(const HugePagedStringStore& other) : 
        BasePagedStringStore(PagedStringStoreType::HUGE), 
        pages(), pageSize(other.pageSize), pagePosition(other.pagePosition), pageAlignment(other.pageAlignment)
    {
        int rc;

        for (char* page: other.pages) {
            rc = posix_memalign((void**) &pageTail, pageAlignment, pageSize);
            if (rc != 0) {
                pagePosition = pageSize + 1;
                throw std::bad_alloc{};
            }

            rc = madvise(pageTail, pageSize, MADV_HUGEPAGE);
            if (rc != 0) {
                pagePosition = pageSize + 1;
                free(pageTail);
                throw std::bad_alloc{};
            }

            std::memcpy(pageTail, page, pageSize);
            pages.push_back(pageTail);
        }
    }

    HugePagedStringStore& HugePagedStringStore::operator=(const HugePagedStringStore& other)
    {
        int rc;

        if (this == &other) {
            return *this;
        }
        
        while(pages.size() > 0) {
            pageTail = pages.back();
            pages.pop_back();
            free(pageTail);
        }

        pageSize = other.pageSize;
        pagePosition = other.pagePosition;
        pageAlignment = other.pageAlignment;
        for (char* page: other.pages) {
            rc = posix_memalign((void**) &pageTail, pageAlignment, pageSize);
            if (rc != 0) {
                pagePosition = pageSize + 1;
                throw std::bad_alloc{};
            }

            rc = madvise(pageTail, pageSize, MADV_HUGEPAGE);
            if (rc != 0) {
                pagePosition = pageSize + 1;
                free(pageTail);
                throw std::bad_alloc{};
            }

            std::memcpy(pageTail, page, pageSize);
            pages.push_back(pageTail);
        }
        
        return *this;
    }

    HugePagedStringStore::HugePagedStringStore(HugePagedStringStore&& other) : 
        BasePagedStringStore(PagedStringStoreType::HUGE), pages(std::move(other.pages)), 
        pageSize(other.pageSize), pagePosition(other.pagePosition), pageAlignment(other.pageAlignment)
    {
        pageTail = other.pageTail;

        other.pages = std::vector<char*>();
        other.pagePosition = 0;
        other.pageTail = NULL;
    }

    HugePagedStringStore& HugePagedStringStore::operator=(HugePagedStringStore&& other)
    {
        if (this == &other) {
            return *this;
        }
        
        while(pages.size() > 0) {
            pageTail = pages.back();
            pages.pop_back();
            delete[] pageTail;
        }

        pageSize = other.pageSize;
        pagePosition = other.pagePosition;
        pages = std::move(other.pages);
        pageTail = other.pageTail;

        other.pages = std::vector<char*>();
        other.pagePosition = 0;
        other.pageTail = NULL;
        
        return *this;
    }

    void HugePagedStringStore::expand()
    {
        int rc;

        rc = posix_memalign((void**) &pageTail, pageAlignment, pageSize);
        if (rc != 0) {
            pagePosition = pageSize + 1;
            throw std::bad_alloc{};
        }

        rc = madvise(pageTail, pageSize, MADV_HUGEPAGE);
        if (rc != 0) {
            pagePosition = pageSize + 1;
            free(pageTail);
            throw std::bad_alloc{};
        }

        std::memset(pageTail, 'x', pageSize);
        pages.push_back(pageTail);
        pagePosition = 0;
    }

    char* HugePagedStringStore::store(const char* str)
    {
        int length = std::strlen(str) + 1;
        char* destination = nullptr;

        if ((pagePosition + length) > pageSize) {
            expand();
        }

        destination = &pageTail[pagePosition];
        std::memcpy(destination, str, length);
        pagePosition += length;

        return destination;
    }
}