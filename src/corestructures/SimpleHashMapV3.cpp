#include "corestructures/SimpleHashMapV3.hpp"

#include <cstring>
#include "utils/hashing.hpp"

namespace ouroboros
{
    SimpleHashMapV3::SimpleHashMapV3(unsigned long numBuckets, BucketManager<char*, long>* bucketManager)
    {
        this->numBuckets = numBuckets;
        this->bucketManager = bucketManager;
        buckets = new bucket_t<char*, long>[numBuckets]{};
    }

    SimpleHashMapV3::~SimpleHashMapV3()
    {
        delete[] buckets;
    }

    inline unsigned long SimpleHashMapV3::hash(char* str)
    {
        return hashFn(str) % numBuckets;
    }

    void SimpleHashMapV3::insert(char *key, long value)
    {
        unsigned long pos = hash(key);
        bucket_t<char*, long> *head = &(buckets[pos]);
        
        if (head->key == NULL) {
            head->key = key;
            head->value = value;
            head->next = NULL;
        } else {
            while (head->next != NULL) {
                if (std::strcmp(head->key, key) == 0) {
                    head->value = value;
                    return;
                }
                head = head->next;
            }
            if (std::strcmp(head->key, key) == 0) {
                head->value = value;
                return;
            }
            head->next = bucketManager->getNewBucket();
            head = head->next;
            head->key = key;
            head->value = value;
            head->next = NULL;
        }
    }

    long SimpleHashMapV3::lookup(char *key)
    {
        unsigned long pos = hash(key);
        bucket_t<char*, long> *head = &(buckets[pos]);

        if (head->key == NULL) {
            return -1;
        }

        while (head != NULL) {
            if (std::strcmp(head->key, key) == 0) {
                return head->value;
            }
            head = head->next;
        }

        return -1;
    }
}