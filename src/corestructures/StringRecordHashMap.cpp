#include "corestructures/StringRecordHashMap.hpp"
#include "utils/hashing.hpp"

namespace ouroboros
{
    StringRecordHashMap::StringRecordHashMap(RecordManager<char*>* recordManager)
    {
        this->recordManager = recordManager;
        numBuckets = OUROBOROS_DEFAULT_STRING_RECORD_HASHMAP_NUM_BUCKETS;
        buckets = new ouroboros_record<char*>*[numBuckets]{NULL};
        numElements = 0;
    }

    StringRecordHashMap::StringRecordHashMap(RecordManager<char*>* recordManager, unsigned long numBuckets)
    {
        this->recordManager = recordManager;
        this->numBuckets = numBuckets;
        buckets = new ouroboros_record<char*>*[numBuckets]{NULL};
        numElements = 0;
    }

    StringRecordHashMap::~StringRecordHashMap()
    {
        delete[] buckets;
    }

    std::tuple<long, bool> StringRecordHashMap::appendToBucket(ouroboros_record<char*>*& bucket, char *str)
    {
        long idx = 0;
        ouroboros_record<char*>* rec;

        if (bucket == NULL) {
            bucket = recordManager->getNewRecord();
            bucket->value = str;
            bucket->next = NULL;
            return std::make_tuple(idx, false);
        } else {
            if (std::strcmp(bucket->value, str) == 0) {
                return std::make_tuple(idx, true);
            }

            rec = bucket;
            while (rec->next != NULL) {
                idx++;
                if (std::strcmp(rec->next->value, str) == 0) {
                    return std::make_tuple(idx, true);
                }
                rec = rec->next;
            }

            rec->next = recordManager->getNewRecord();
            idx++;
            rec->next->value = str;
            rec->next->next = NULL;
            return std::make_tuple(idx, false);
        }
    }

    std::tuple<long, bool> StringRecordHashMap::lookupInBucket(ouroboros_record<char*>* bucket, char* str)
    {
        long idx;
        ouroboros_record<char*>* rec;

        rec = bucket;
        while (rec != NULL) {
            if (std::strcmp(rec->value, str) == 0) {
                return std::make_tuple(idx, true);
            }
            idx++;
            rec = rec->next;
        }

        return std::make_tuple(-1, false);
    }

    inline unsigned long StringRecordHashMap::hash(const char* str)
    {
        unsigned long hash_value = djb2_hash((const unsigned char*) str);
        return hash_value % numBuckets;
    }

    std::tuple<long, bool> StringRecordHashMap::insert(char* str)
    {

        unsigned long bucketPos = hash(str);
        std::tuple<long, bool> idx_exists;

        idx_exists = appendToBucket(buckets[bucketPos], str);
        if (!std::get<1>(idx_exists)) {
            numElements++;
        }

        return idx_exists;
    }

    std::tuple<long, bool> StringRecordHashMap::lookup(char* str)
    {
        unsigned long bucketPos = hash(str);

        return lookupInBucket(buckets[bucketPos], str);
    }

    long StringRecordHashMap::getNumElements()
    {
        return numElements;
    }
}