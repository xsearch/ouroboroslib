#include "corestructures/StringHashMap.hpp"
#include "utils/hashing.hpp"

namespace ouroboros
{
    StringHashMap::StringHashMap(unsigned long numBuckets, long minCollisions, long maxCollisions)
    {
        this->numBuckets = numBuckets;
        this->minCollisions = minCollisions;
        this->maxCollisions = maxCollisions;
        buckets = new CapacityArray<char*>*[numBuckets]{NULL};
        mtxs = new std::mutex[numBuckets]();
        numElements.store(0);
    }

    StringHashMap::~StringHashMap()
    {
        for (unsigned long i = 0; i < numBuckets; i++) {
            if (buckets[i] != NULL) {
                delete buckets[i];
            }
        }
        delete buckets;
        delete mtxs;
    }

    inline unsigned long StringHashMap::hash(const char *str)
    {
        unsigned long hash_value = djb2_hash((const unsigned char*) str);
        return hash_value % numBuckets;
    }

    std::tuple<long, bool> StringHashMap::insert(char *str)
    {
        unsigned long bucketPos = hash(str);
        long length;
        long idx;
        bool exists = false;
        char *s;

        if (buckets[bucketPos] == NULL) {
            std::lock_guard<std::mutex> lock(mtxs[bucketPos]);
            if (buckets[bucketPos] == NULL) {
                buckets[bucketPos] = new CapacityArray<char*>(minCollisions, maxCollisions);
            }
        }
        length = buckets[bucketPos]->getLength();

        if (length > 0) {
            for (auto i = 0; i < length; i++) {
                s = (*buckets[bucketPos])[i];
                if (std::strcmp(str, s) == 0) {
                    idx = i;
                    exists = true;
                    break;
                }
            }
        }
        
        if (!exists) {
            idx = buckets[bucketPos]->push_back(str);
            numElements++;
        }

        return std::make_tuple(idx, exists);
    }

    std::tuple<long, bool> StringHashMap::lookup(char *str)
    {
        unsigned long bucketPos = hash(str);
        long length;
        long idx = -1;
        bool exists = false;
        char *s;

        if (buckets[bucketPos] == NULL) {
            return std::make_tuple(idx, exists);
        }
        length = buckets[bucketPos]->getLength();

        if (length > 0) {
            for (auto i = 0; i < length; i++) {
                s = (*buckets[bucketPos])[i];
                if (std::strcmp(str, s) == 0) {
                    idx = i;
                    exists = true;
                    break;
                }
            }
        }

        return std::make_tuple(idx, exists);
    }

    long StringHashMap::getNumElements()
    {
        return numElements.load();
    }
}