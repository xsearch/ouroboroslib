#include "corestructures/SimpleHashMapV4.hpp"

#include <cstring>

namespace ouroboros
{
    SimpleHashMapV4::SimpleHashMapV4(unsigned long numBuckets, unsigned long bucketSize)
    {
        this->numBuckets = numBuckets;
        this->bucketSize = bucketSize;
        rawBuckets = new char[numBuckets * bucketSize]{};
    }

    SimpleHashMapV4::~SimpleHashMapV4()
    {
        delete[] rawBuckets;
    }

    inline unsigned long SimpleHashMapV4::hash(char* str)
    {
        return hashFn(str) % numBuckets;
    }

    void SimpleHashMapV4::insert(char *key, long value)
    {
        unsigned long pos = hash(key) * bucketSize;
        char *bucket = &(rawBuckets[pos]);
        unsigned long i = 0;
        unsigned long j = 0;
        unsigned long K = sizeof(long);
        unsigned long N = std::strlen(key) + 1;
        unsigned long M = bucketSize;
        bool condEqual;
        bool condNullByte;
        bool condNullBytePrev;
        bool condTerminate = false;
        
        condNullBytePrev = (bucket[0] == '\0');
        condNullByte = (bucket[i] == '\0');
        if (!condNullBytePrev) {
            while (i < M && j < N && !condTerminate) {
                condEqual = (bucket[i] == key[j]);
                j = (condEqual) ? (j + 1) : 0;
                i = (condNullByte) ? (i + K + 1) : (i + 1);
                condNullBytePrev = condNullByte;
                condNullByte = (bucket[i] == '\0');
                condTerminate = (condNullByte && condNullBytePrev);
            }
        }

        if (j != N && (i + N + K) < M) {
            std::memcpy(&bucket[i], key, N);
            std::memcpy(&bucket[i + N], &value, K);
        }
    }

    long SimpleHashMapV4::lookup(char *key)
    {
        unsigned long pos = hash(key) * bucketSize;
        char *bucket = &(rawBuckets[pos]);
        unsigned long i = 0;
        unsigned long j = 0;
        unsigned long K = sizeof(long);
        unsigned long N = std::strlen(key) + 1;
        unsigned long M = bucketSize;
        bool condEqual;
        bool condNullByte;
        bool condNullBytePrev;
        bool condTerminate = false;
        long value = -1;

        condNullBytePrev = (bucket[0] == '\0');
        condNullByte = (bucket[i] == '\0');
        if (!condNullBytePrev) {
            while (i < M && j < N && !condTerminate) {
                condEqual = (bucket[i] == key[j]);
                j = (condEqual) ? (j + 1) : 0;
                i = (condNullByte) ? (i + K + 1) : (i + 1);
                condNullBytePrev = condNullByte;
                condNullByte = (bucket[i] == '\0');
                condTerminate = (condNullByte && condNullBytePrev);
            }
        }

        if (j == N) {
            std::memcpy(&value, &bucket[i - K], K);
        }

        return value;
    }
}