#include "corestructures/RandomPagedStringStore.hpp"

#include <cstring>
#include <cstdlib>
#include <chrono>
#include <random>
#include <new>

extern "C"
{
    #include <sys/mman.h>
}

namespace ouroboros
{
    RandomPagedStringStore::RandomPagedStringStore(long pageSize) : BasePagedStringStore(PagedStringStoreType::RANDOM)
    {
        long i, j;
        int rc;
        
        this->pageSize = pageSize;
        pages = new std::vector<char*>();
        rc = posix_memalign((void**) &pageTail, HUGE_PAGE_ALIGNMENT, pageSize);
        if (rc != 0) {
            allocated = false;
            pagePosition = pageSize + 1;
            throw std::bad_alloc{};
        }

        rc = madvise(pageTail, pageSize, MADV_HUGEPAGE);
        if (rc != 0) {
            allocated = false;
            pagePosition = pageSize + 1;
            free(pageTail);
            throw std::bad_alloc{};
        }
        
        allocated = true;
        pages->push_back(pageTail);
        pagePosition = 0;
        std::memset(pageTail, 'x', pageSize);

        shufflePatternSize = (pageSize - STRING_MAX_LENGTH) / STRING_DEFAULT_LENGTH;

        unsigned long seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::mt19937_64 random_number_generator(seed);
        std::uniform_int_distribution<long> distribution(0, shufflePatternSize);

        shufflePattern = new long[shufflePatternSize];
        for (i = 0; i < shufflePatternSize; i++) {
            shufflePattern[i] = i * STRING_DEFAULT_LENGTH;
        }

        for (i = 0; i < shufflePatternSize; i++) {
            j = distribution(random_number_generator);
            std::swap(shufflePattern[i], shufflePattern[j]);
        }
        shufflePosition = 0;
    }

    RandomPagedStringStore::~RandomPagedStringStore()
    {
        while(pages->size() > 0) {
            pageTail = pages->back();
            pages->pop_back();
            free(pageTail);
        }
        delete pages;
        delete shufflePattern;
    }

    bool RandomPagedStringStore::isAllocated()
    {
        return allocated;
    }

    void RandomPagedStringStore::expand()
    {
        int rc;

        rc = posix_memalign((void**) &pageTail, HUGE_PAGE_ALIGNMENT, pageSize);
        if (rc != 0) {
            allocated = false;
            pagePosition = pageSize + 1;
            throw std::bad_alloc{};
        }

        rc = madvise(pageTail, pageSize, MADV_HUGEPAGE);
        if (rc != 0) {
            allocated = false;
            pagePosition = pageSize + 1;
            free(pageTail);
            throw std::bad_alloc{};
        }
        pages->push_back(pageTail);
        pagePosition = 0;
        std::memset(pageTail, 'x', pageSize);

        shufflePosition = 0;
    }

    char* RandomPagedStringStore::store(const char* str)
    {
        int length = std::strlen(str) + 1;
        char* destination = nullptr;

        if ((pagePosition + length) > pageSize) {
            expand();
        }

        destination = &pageTail[shufflePattern[shufflePosition]];
        std::memcpy(destination, str, length);
        pagePosition += length;
        shufflePosition = (shufflePosition + 1) % shufflePatternSize;

        return destination;
    }
}