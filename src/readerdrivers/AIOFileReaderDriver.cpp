#include "readerdrivers/AIOFileReaderDriver.hpp"

#include <iostream>
#include <cstring>
#include <cerrno>

namespace linuxIO
{
    extern "C" {
        #include <stdlib.h>
        #include <unistd.h>
        #include <fcntl.h>
        #include <aio.h>
        #include <signal.h>
    }
}

namespace ouroboros
{
    const char* AIOFileReaderException::what() const throw() {
        return "ERROR: could not open file!";
    }

    AIOFileReaderDriver::AIOFileReaderDriver(char *filepath, int blockSize, char *buffer, int cacheNumBlocks): 
            BaseReaderDriver(ReaderDriverType::PFILE) {
        this->filepath = filepath;
        position = 0;
        cachePosition = cacheNumBlocks;
        this->blockSize = blockSize;
        cacheSize = cacheNumBlocks;
        
        cacheBuffer = buffer;

        aiocbs = (linuxIO::aiocb_t*) linuxIO::calloc(cacheSize, sizeof(linuxIO::aiocb_t));
        if (aiocbs == NULL) {
            throw AIOFileReaderException("ERROR: could not allocate internal control blocks!");
        }

        aiocbsList = (linuxIO::aiocb_t**) linuxIO::calloc(1, sizeof(linuxIO::aiocb_t*));
        if (aiocbsList == NULL) {
            free(aiocbs);
            throw AIOFileReaderException("ERROR: could not allocate internal control blocks!");
        }
    }

    void AIOFileReaderDriver::open() {
        for (auto i = 0; i < cacheSize; i++) {
            aiocbs[i].aio_fildes = linuxIO::open(filepath, O_RDONLY | O_DIRECT);
            if (aiocbs[i].aio_fildes < 0) {
                throw AIOFileReaderException("ERROR: could not open " + std::string(filepath) + " !");
            }
            linuxIO::posix_fadvise(aiocbs[i].aio_fildes, 0, 0, POSIX_FADV_RANDOM | POSIX_FADV_DONTNEED);
            aiocbs[i].aio_buf = &cacheBuffer[i * blockSize];
            aiocbs[i].aio_nbytes = blockSize;
            aiocbs[i].aio_reqprio = 0;
            aiocbs[i].aio_sigevent.sigev_notify = linuxIO::SIGEV_NONE;
        }

        cachePosition = cacheSize;
    }

    void AIOFileReaderDriver::close() {
        int rc;

        for (auto i = 0; i < cacheSize; i++) {
            if (aiocbs[i].aio_fildes > 0) {
                rc = linuxIO::close(aiocbs[i].aio_fildes);
                if (rc == -1) {
                    throw AIOFileReaderException("ERROR: could not close " + std::string(filepath) + " !");
                }
            }
        }
    }

    inline void AIOFileReaderDriver::readNextBlock(BaseDataBlock *dataBlock) {
        readNextBlock((FileDataBlock*) dataBlock);
    }

    void AIOFileReaderDriver::readNextBlock(FileDataBlock *dataBlock) {
        int rc;
        long offset;
        char *bp;

        if (cachePosition >= cacheSize) {
            for (auto i = 0; i < cacheSize; i++) {
                offset = (long) position * (long) blockSize;
                aiocbs[i].aio_offset = offset;
                rc = linuxIO::aio_read(&aiocbs[i]);
                if (rc != 0) {
                    std::cout << "WARN: aio_read operation failed!" << std::endl;
                }
                position++;
            }
            cachePosition = 0;
        }

        while ((rc = linuxIO::aio_error(&(aiocbs[cachePosition]))) == EINPROGRESS) {
            aiocbsList[0] = &(aiocbs[cachePosition]);
            linuxIO::aio_suspend(aiocbsList, 1, NULL);
        }

        if (rc == 0) {
            rc = linuxIO::aio_return(&aiocbs[cachePosition]);
            
            bp = dataBlock->buffer;
            dataBlock->buffer = (char*) (aiocbs[cachePosition].aio_buf);
            aiocbs[cachePosition].aio_buf = bp;

            dataBlock->position = position - blockSize + cachePosition;
            dataBlock->blockSize = blockSize;
            dataBlock->length = rc;
            dataBlock->filepath = filepath;
        } else {
            std::cout << "WARN: aio_read operation failed (aio_error)!" << std::endl;
        }

        cachePosition++;
    }

    AIOFileReaderDriver::~AIOFileReaderDriver() {
        if (aiocbs != NULL) {
            linuxIO::free(aiocbs);
        }

        if (aiocbsList != NULL) {
            linuxIO::free(aiocbsList);
        }
    }
}