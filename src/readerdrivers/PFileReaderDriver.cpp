#include "readerdrivers/PFileReaderDriver.hpp"

#include <iostream>

extern "C" {
    #include <unistd.h>
    #include <fcntl.h>
}

namespace ouroboros
{
    PFileReaderDriver::PFileReaderDriver(char *filepath, int blockSize): BaseReaderDriver(ReaderDriverType::PFILE) {
        this->filepath = filepath;
        fd = -1;
        position = 0;
        this->blockSize = blockSize;
    }

    void PFileReaderDriver::open() {
        fd = ::open(filepath, O_RDONLY | O_DIRECT);
        if (fd == -1) {
            throw PFileReaderException("ERROR: could not open " + std::string(filepath) + " !");
        }
        ::posix_fadvise(fd, 0, 0, POSIX_FADV_SEQUENTIAL);
    }

    void PFileReaderDriver::close() {
        int rc;
        if (fd != -1) {
            rc = ::close(fd);
            if (rc == -1) {
                throw PFileReaderException("ERROR: could not close " + std::string(filepath) + " !");
            }
        }
    }

    inline void PFileReaderDriver::readNextBlock(BaseDataBlock *dataBlock) {
        readNextBlock((FileDataBlock*) dataBlock);
    }

    inline void PFileReaderDriver::readNextBlock(FileDataBlock *dataBlock) {
        int position = (this->position).fetch_add(1, std::memory_order_seq_cst);
        readBlockAt(dataBlock, position);
    }

    void PFileReaderDriver::readBlockAt(FileDataBlock *dataBlock, int position) {
        int rc;
        long offset;

        offset = (long) position * (long) blockSize;
        rc = ::pread(fd, dataBlock->buffer, blockSize, offset);
        
        dataBlock->position = position;
        dataBlock->blockSize = blockSize;
        dataBlock->length = rc;
        dataBlock->filepath = filepath;
    }
}
