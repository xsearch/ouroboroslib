#include "readerdrivers/FileDataBlock.hpp"

namespace ouroboros
{
    FileDataBlock::FileDataBlock(): BaseDataBlock(DataBlockType::FILE) {
        this->buffer = nullptr;
        this->filepath = nullptr;
    }
}