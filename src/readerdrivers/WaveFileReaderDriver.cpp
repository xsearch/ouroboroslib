#include "readerdrivers/WaveFileReaderDriver.hpp"
//#include "utils/linux-api.hpp"

#include <iostream>
#include <cstring>
#include <new>
#include <unistd.h>
#include <fcntl.h>

namespace ouroboros
{
    WaveFileReaderDriver::WaveFileReaderDriver(char *filepath, int blockSize, int blockAddOn, char* delims) : 
        BaseReaderDriver(ReaderDriverType::WFILE), 
        filepath(filepath), fd(NONE), blockPos(0), blockSize(blockSize), blockAddOn(blockAddOn)
    {
        for (int i = 0; i < 256; i++) {
            this->delimDict[i] = 1;
        }
        for (long unsigned int i = 0; i < std::strlen(delims); i++) {
            this->delimDict[(int) delims[i]] = 0;
        }
        delim = delims[0];
    }
    
    WaveFileReaderDriver::~WaveFileReaderDriver()
    {
        if (fd != NONE) {
            try {
                WaveFileReaderDriver::close();
            } catch (WaveFileReaderException &e) {
                std::cerr << e.what() << std::endl;
            }
        }
    }

    WaveFileReaderDriver::WaveFileReaderDriver(const WaveFileReaderDriver& other) : 
        BaseReaderDriver(ReaderDriverType::WFILE), filepath(other.filepath), 
        fd(NONE), blockPos(other.blockPos), blockSize(other.blockSize), blockAddOn(other.blockAddOn)
    {
        for (int i = 0; i < 256; i++) {
            this->delimDict[i] = other.delimDict[i];
        }
        this->delim = other.delim;

        if (other.fd != NONE) {
            WaveFileReaderDriver::open();
        }
    }

    WaveFileReaderDriver& WaveFileReaderDriver::operator=(const WaveFileReaderDriver& other)
    {
        if (this == &other) {
            return *this;
        }

        if (this->fd != NONE) {
            try {
                WaveFileReaderDriver::close();
            } catch (WaveFileReaderException &e) {
                std::cerr << e.what() << std::endl;
            }
        }

        this->filepath = other.filepath;
        this->fd = NONE;
        this->blockPos = other.blockPos;
        this->blockSize = other.blockSize;
        this->blockAddOn = other.blockAddOn;

        for (int i = 0; i < 256; i++) {
            this->delimDict[i] = other.delimDict[i];
        }
        this->delim = other.delim;
        
        if (other.fd != NONE) {
            WaveFileReaderDriver::open();
        }

        return *this;
    }

    WaveFileReaderDriver::WaveFileReaderDriver(WaveFileReaderDriver&& other) : 
        BaseReaderDriver(ReaderDriverType::WFILE), filepath(other.filepath), 
        fd(other.fd), blockPos(other.blockPos), blockSize(other.blockSize), blockAddOn(other.blockAddOn)
    {
        for (int i = 0; i < 256; i++) {
            this->delimDict[i] = other.delimDict[i];
        }
        this->delim = other.delim;

        other.fd = NONE;
    }

    WaveFileReaderDriver& WaveFileReaderDriver::operator=(WaveFileReaderDriver&& other)
    {
        if (this == &other) {
            return *this;
        }

        if (this->fd != NONE) {
            try {
                WaveFileReaderDriver::close();
            } catch (WaveFileReaderException &e) {
                std::cerr << e.what() << std::endl;
            }
        }

        this->filepath = other.filepath;
        this->fd = other.fd;
        this->blockPos = other.blockPos;
        this->blockSize = other.blockSize;
        this->blockAddOn = other.blockAddOn;

        for (int i = 0; i < 256; i++) {
            this->delimDict[i] = other.delimDict[i];
        }
        this->delim = other.delim;

        other.fd = NONE;

        return *this;
    }

    void WaveFileReaderDriver::open()
    {
        if (fd == NONE) {
            fd = ::open(filepath, O_RDONLY | O_DIRECT);
            if (fd == -1) {
                throw WaveFileReaderException("ERROR: could not open " + std::string(filepath) + " !");
            }
            ::posix_fadvise(fd, 0, 0, POSIX_FADV_SEQUENTIAL);
        } else {
            throw WaveFileReaderException("ERROR: file " + std::string(filepath) + " already opened !");
        }
    }

    void WaveFileReaderDriver::close()
    {
        int rc;

        if (fd != NONE) {
            rc = ::close(fd);
            if (rc == -1) {
                throw WaveFileReaderException("ERROR: could not close " + std::string(filepath) + " !");
            }
            fd = NONE;
        }
    }

    inline void WaveFileReaderDriver::readNextBlock(BaseDataBlock *dataBlock)
    {
        WaveFileReaderDriver::readNextBlock((FileDataBlock*) dataBlock);
    }

    inline void WaveFileReaderDriver::readNextBlock(FileDataBlock *dataBlock)
    {
        WaveFileReaderDriver::readBlockAt(dataBlock, blockPos);
    }

    void WaveFileReaderDriver::readBlockAt(FileDataBlock *dataBlock, int blockNumber)
    {
        int rc, i, j;
        long offset;

        blockPos = blockNumber;
        offset = (long) blockPos * (long) blockSize;
        rc = ::pread(fd, dataBlock->buffer, blockSize + blockAddOn, offset);

        if (rc < 0) {
            std::cout << "WARN: pread failed in " << std::string(filepath) << " at block position "  
                << blockPos << std::endl;
        }
        
        j = 0;
        if (blockPos > 0) {
            while (delimDict[(int) dataBlock->buffer[j]] and j < rc) {
                dataBlock->buffer[j] = delim;
                j++;
            }
        }
        
        if (rc > blockSize) {
            i = blockSize;
            while (i < rc and delimDict[(int) dataBlock->buffer[i]]) {
                i++;
            }

            if (i == rc) {
                std::cout << "WARN: term larger than " << blockAddOn << " addon bytes detected in " 
                    << std::string(filepath) << " at block position "  << blockPos << std::endl;
            }
        } else {
            i = rc;
        }

        dataBlock->length = i;
        dataBlock->position = blockPos++;
        dataBlock->blockSize = blockSize + blockAddOn;
        dataBlock->filepath = filepath;
    }
}
