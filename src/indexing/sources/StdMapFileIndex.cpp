#include "indexing/sources/StdMapFileIndex.hpp"

namespace ouroboros
{
    long StdMapFileIndex::insert(const char *path)
    {
        long idx = -1;

        auto search = map.find(path);
        if (search == map.end()) {
            paths.push_back(path);
            idx = numFiles++;
            map.insert({path, idx});
        } else {
            idx = search->second;
        }

        return idx;
    }

    long StdMapFileIndex::lookup(const char *path)
    {
        long idx = -1;

        auto search = map.find(path);
        if (search != map.end()) {
            idx = search->second;
        }

        return idx;
    }

    const char* StdMapFileIndex::reverseLookup(long idx)
    {
        if (idx >= numFiles) {
            return NULL;
        }

        return paths.at(idx);
    }

}