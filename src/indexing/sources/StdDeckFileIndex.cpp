#include "indexing/sources/StdDeckFileIndex.hpp"
#include <cstring>

namespace ouroboros
{
    long StdDeckFileIndex::insert(const char *path)
    {
        files.push_back(path);
        return idx++;
    }

    long StdDeckFileIndex::lookup(const char *path)
    {
        for (long idx = 0; idx < files.size(); idx++) {
            if (std::strcmp(files.at(idx), path) == 0) {
                return idx;
            }
        }

        return -1;
    }

    const char* StdDeckFileIndex::reverseLookup(long idx)
    {
        return files.at(idx);
    }
}