#include "indexing/sources/StdAppendFileIndex.hpp"

namespace ouroboros
{
    StdAppendFileIndex::StdAppendFileIndex()
    {
        files = new std::vector<std::string*>();
    }

    StdAppendFileIndex::~StdAppendFileIndex()
    {
        for (std::string* elem : *files) {
            delete elem;
        }
        delete files;
    }

    long StdAppendFileIndex::insert(const char *path)
    {
        long fileIdx;
        std::string* elem;
        
        elem = new std::string(path);
        files->push_back(elem);
        fileIdx = files->size() - 1;
        
        return fileIdx;
    }

    long StdAppendFileIndex::lookup(const char *path)
    {
        long fileIdx = -1;
        std::string elem(path);

        for (unsigned int i = 0; i < files->size(); i++) {
            if ((*(*files)[i]) == elem) {
                fileIdx = i;
                break;
            }
        }

        return fileIdx;
    }

    const char* StdAppendFileIndex::reverseLookup(long idx)
    {
        std::string* elem = files->at(idx);

        return elem->c_str();
    }
}