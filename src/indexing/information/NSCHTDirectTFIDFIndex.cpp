#include "indexing/information/NSCHTDirectTFIDFIndex.hpp"

namespace ouroboros
{
    NSCHTDirectTFIDFIndex::NSCHTDirectTFIDFIndex(long pageSize, long numBuckets) \
            : BaseInformationIndex(InformationIndexType::DIRECTTFIDF)
    {
        store = new NSMemBytesStore(pageSize);
        map = new ChainedHashTable<char*, long, charptr_hash, charptr_equal>(numBuckets);
        terms = new std::vector<tfidf_data_t>();
    }
    
    NSCHTDirectTFIDFIndex::~NSCHTDirectTFIDFIndex()
    {
        delete map;
        delete terms;
        delete store; 
    }

    long NSCHTDirectTFIDFIndex::insert(const char *term, long fileIdx)
    {
        char* termCopy;
        long idx = -1;
        tfidf_data_t data{};

        if (map->lookup((char*) term, idx) == false) {
            termCopy = store->copy(term);
            data.term = termCopy;
            data.fileIdx = fileIdx;
            data.frequency = 1;
            terms->push_back(data);
            idx = terms->size() - 1;
            map->insert(termCopy, idx);
            store->next();
        } else {
            data = terms->at(idx);
            data.frequency++;
        }

        return idx;
    }

    long NSCHTDirectTFIDFIndex::lookup(const char *term)
    {
        long idx = -1;

        map->lookup((char*) term, idx);

        return idx;
    }

    const std::tuple<char*, long, long> NSCHTDirectTFIDFIndex::reverseLookup(long idx)
    {
        tfidf_data_t data{};

        data = terms->at(idx);

        return std::make_tuple(data.term, data.fileIdx, data.frequency);
    }
}