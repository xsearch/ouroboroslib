#include "indexing/data/DenseMapTermIndex.hpp"

#include <iostream>

#ifdef GOOGLE_DENSE_LIB

namespace ouroboros
{
    long DenseMapTermIndex::insert(const char* term)
    {
        char* storeTerm;
        long idx = -1;
        
        auto search = map.find(term);
        if (search == map.end()) {
            storeTerm = store->store(term);
            terms.push_back(storeTerm);
            idx = numTerms++;
            map.insert({storeTerm, idx});
        } else {
            idx = search->second;
        }

        return idx;
    }

    void DenseMapTermIndex::remove(const char *term)
    {

    }

    long DenseMapTermIndex::lookup(const char *term)
    {
        long idx = -1;

        auto search = map.find(term);
        if (search != map.end()) {
            idx = search->second;
        }

        return idx;
    }

    const char* DenseMapTermIndex::reverseLookup(long idx)
    {
        if (idx >= numTerms) {
            return NULL;
        }

        return terms.at(idx);
    }

    long DenseMapTermIndex::getNumTerms()
    {
        return numTerms;
    }

}

#endif