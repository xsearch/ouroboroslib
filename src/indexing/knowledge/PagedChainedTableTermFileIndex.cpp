#include "indexing/knowledge/PagedChainedTableTermFileIndex.hpp"

#include <iostream>

namespace ouroboros
{
    long PagedChainedTableTermFileIndex::insert(const char *term, long fileIdx)
    {
        PagedVersatileIndex<char*, long>* entry;
        long idx = -1;

        if (!map.lookup(term, idx)) {
            entry = indexStore->getNew();
            entry->element = termStore->store(term);
            entry->tail = listStore->getNew();
            entry->tail->element = fileIdx;
            entry->tail->next = NULL;
            entry->head = entry->head;
            index.push_back(entry);
            idx = numTerms++;
            map.insert(entry->element, idx);
        } else {
            entry = nativeReverseLookup(idx);
            if (entry != NULL and entry->tail->element != fileIdx) {
                entry->tail->next = listStore->getNew();
                entry->tail->next->element = fileIdx;
                entry->tail->next->next = NULL;
                entry->tail = entry->tail->next;
            }
        }

        return idx;
    }

    void PagedChainedTableTermFileIndex::remove(const char *term, long fileIdx)
    {

    }

    long PagedChainedTableTermFileIndex::lookup(const char *term)
    {
        long idx = -1;

        map.lookup(term, idx);

        return idx;
    }

    std::shared_ptr<TermFileIndexEntry> PagedChainedTableTermFileIndex::reverseLookup(long idx)
    {
        std::shared_ptr<TermFileIndexEntry> rcEntry;
        PagedVersatileIndex<char*, long>* entry;
        PagedLinkedElement<long>* p;

        if (idx >= numTerms) {
            rcEntry = std::shared_ptr<TermFileIndexEntry>(new TermFileIndexEntry(NULL));
        } else {
            entry = index.at(idx);
            rcEntry = std::shared_ptr<TermFileIndexEntry>(new TermFileIndexEntry(entry->element));
            p = entry->head;
            while (p != NULL) {
                rcEntry->files.push_back(p->element);
                p = p->next;
            }
        }
        
        return rcEntry;
    }

    PagedVersatileIndex<char*, long>* PagedChainedTableTermFileIndex::nativeReverseLookup(long idx)
    {
        PagedVersatileIndex<char*, long>* entry;

        if (idx >= numTerms) {
            entry = NULL;
        } else {
            entry = index.at(idx);
        }

        return entry;
    }

    long PagedChainedTableTermFileIndex::getNumTerms()
    {
        return numTerms;
    }

}