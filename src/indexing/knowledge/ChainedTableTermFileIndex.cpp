#include "indexing/knowledge/ChainedTableTermFileIndex.hpp"

namespace ouroboros
{
    long ChainedTableTermFileIndex::insert(const char *term, long fileIdx)
    {
        std::shared_ptr<TermFileIndexEntry> entry;
        long idx = -1;

        if (!map.lookup(term, idx)) {
            entry = std::shared_ptr<TermFileIndexEntry>(new TermFileIndexEntry(store->store(term)));
            index.push_back(entry);
            idx = numTerms++;
            map.insert(entry->term, idx);
            entry->files.push_back(fileIdx);
        } else {
            entry = reverseLookup(idx);
            if (entry->files.back() != fileIdx) {
                entry->files.push_back(fileIdx);
            }
        }

        return idx;
    }

    void ChainedTableTermFileIndex::remove(const char *term, long fileIdx)
    {

    }

    long ChainedTableTermFileIndex::lookup(const char *term)
    {
        long idx = -1;

        map.lookup(term, idx);

        return idx;
    }

    std::shared_ptr<TermFileIndexEntry> ChainedTableTermFileIndex::reverseLookup(long idx)
    {
        std::shared_ptr<TermFileIndexEntry> entry;

        if (idx >= numTerms) {
            entry = std::shared_ptr<TermFileIndexEntry>(new TermFileIndexEntry(NULL));
        } else {
            entry = index.at(idx);
        }
        
        return entry;
    }

    long ChainedTableTermFileIndex::getNumTerms()
    {
        return numTerms;
    }

}